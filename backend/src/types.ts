import { Db } from "mongodb";
import { Worker } from "worker_threads";

export interface AnkiCardRef {
    repetition: number;
    interval: number;
    efactor: number;
    passedAt: Date | undefined;
}

export interface AnkiConfig {
    deckNames: string[],
    decks: Map<string, Map<string, AnkiCardRef>>
}

export interface ServiceResponse {
    status: number;
    headers: { [key: string]: string };
    body: string;
}

interface Dict {
    [k: string]: string | string[] | undefined;
}

export interface Request {
    method: string;
    url: string;
    params: Dict;
    query: Dict;
    headers: Dict;
    body: any;
}

export interface Response {
    status: number;
    headers: Dict;
    body: any;
}

export interface BasicState {
    [k: string]: any;
}

export interface HookContext<T extends BasicState> {
    request: Request;
    response: Response;
    state: T;
}

export type HookHandler<T extends BasicState> = (ctx: HookContext<T>) => Promise<any>;

export interface User {
    nickname: string;
    email: string;
    passwordConfig: PasswordConfig;
    confirmationCode: string;
    confirmed: boolean;
}

export interface UserSession {
    nickname: string;
    sessionCode: string;
}

export interface PassRecoverageSession {
    nickname: string;
    sessionCode: string;
}

export interface State {
    db: Db;
    sessionsStorage: Db;
    templatesStorage: Db;
    resourcesPath: string;
    fallbackPage: string;
    user?: User;
    mailer: Worker;
}

export enum PassAlgorithm {
    Concat = 0,
    Merge = 1,
    ReverseAndConcat = 2,
    ReverseAndMerge = 3,
}

export interface PasswordConfig {
    hash: string;
    salt: string;
    algorithm: PassAlgorithm;
}

export interface Deck {
    uid: string;
    title: string;
    author: string;
}

export interface Card {
    uid: string;
    title: string;
    author: string;
    deckUid: string;
    repetition: number;
    interval: number;
    efactor: number;
    passedAt: string | null;
}

export interface MailerMessage {
    action: "send";
    payload: {
        email: string;
        message: string;
    };
}

export interface CardTemplate {
    uid: string;
    deckUid: string;
    author: string;
    frontSide: string;
    backSide: string;
}
