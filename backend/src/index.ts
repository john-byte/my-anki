import http from "http";
import path from "path";
import fs from "fs";
import { MongoClient } from "mongodb";
import { Worker } from "worker_threads";
import { App } from "./app";
import { State, UserSession, User } from "./types";

import { cors } from "./hooks/cors";
import { parseJson, stringifyJson } from "./hooks/json";
import { createAnkiRoutes } from "./routes/anki";
import { createAuthRoutes } from "./routes/auth";
import { createCmsDecksRoutes } from "./routes/cms-decks";
import { createCmsCardsRoutes } from './routes/cms-cards';

const main = async () => {
    const client = await MongoClient.connect(process.env.MONGODB_URL!);
    const db = client.db("anki");
    const sessionStorage = client.db("anki-sessions");
    const templatesStorage = client.db("anki-templates");
    const resourcesPath = path.join(
        __dirname,
        "..",
        "..",
        process.env.PATH_RESOURCES!
    );

    const fallbackPage = await fs.promises.readFile(
        path.join(
            resourcesPath,
            process.env.PATH_GET_TEMPLATE!
        ),
        {
            encoding: "utf-8"
        }
    );

    const mailer = new Worker(path.resolve(__dirname, "mailer.js"));

    const app = new App<State>();
    app.use(cors);
    app.use(parseJson);

    // 1. Move initialized resources into context
    app.use(async (ctx) => {
        ctx.state.db = db;
        ctx.state.sessionsStorage = sessionStorage;
        ctx.state.templatesStorage = templatesStorage;
        ctx.state.resourcesPath = resourcesPath;
        ctx.state.fallbackPage = fallbackPage;
        ctx.state.user = undefined;
        ctx.state.mailer = mailer;
    });

    // 2. Attempt to find user by session code
    app.use(async (ctx) => {
        const sessionsStore = ctx.state.sessionsStorage;
        const rawSessCode = ctx.request.headers["x-sesscode"];
        const sessionCode = Array.isArray(rawSessCode) ? rawSessCode[0] : rawSessCode;

        if (sessionCode) {
            const session = await sessionsStore.collection<UserSession>("sessions")
                .findOne({
                    sessionCode,
                });

            if (session) {
                const db = ctx.state.db;
                const user = await db.collection<User>("users").findOne({
                    nickname: session.nickname,
                });

                if (user) {
                    ctx.state.user = user;
                }
            }
        }
    });

    app.use(createAnkiRoutes());
    app.use(createAuthRoutes());
    app.use(createCmsDecksRoutes());
    app.use(createCmsCardsRoutes());
    app.use(stringifyJson);

    const server = http.createServer(app.toReqListener());
    server.listen(
        process.env.PORT,
        () => {
            console.log(`Listening on port ${process.env.PORT}`);
        }
    );
}

main().catch(e => console.error(e));
