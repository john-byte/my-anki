import { Router } from "../../router";
import { State, User, Deck, Card, CardTemplate } from "../../types";
import { ctxAssert } from "../../utils/ctx-assert";
import { requireAuth } from "../../utils/require-auth";

export const createCmsDecksRoutes = () => {
    const router = new Router<State>("/cms/decks");

    router.get("/all", async (ctx) => {
        let user: User;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        const decksColl = ctx.state.db.collection<Deck>("decks");
        const decks = await decksColl.find({
            author: user.nickname,
        }).toArray();

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers["content-type"] = "application/json";
        ctx.response.body = decks;
    });

    router.post("/add", async (ctx) => {
        let user: User;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        let title = ctx.request.body?.title;
        if (typeof title === "string") {
            title = title.trim();
        }
        try {
            ctxAssert(
                ctx,
                typeof title === "string" && title.length > 0,
                "Title should not be empty!"
            );
        } catch (_) {
            return;
        }

        const decksColl = ctx.state.db.collection<Deck>("decks");
        // TODO: replace slug with number id.
        // String uid exists for historical reason,
        // when Anki was a single user system
        const uid = `${user.nickname}__${(new Date()).getTime()}`;
        const result = await decksColl.insertOne({
            uid,
            title,
            author: user.nickname,
        });

        if (result.result.ok) {
            ctx.response.status = Number(process.env.STATUS_OK!);
            ctx.response.headers["content-type"] = "application/json";
            ctx.response.body = {
                success: true,
            };
        } else {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "DB temporary unavailable";
        }
    });

    router.post("/edit", async (ctx) => {
        let user: User;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        const uid = ctx.request.body?.uid;
        let title = ctx.request.body?.title;
        if (typeof title === "string") {
            title = title.trim();
        }
        try {
            ctxAssert(
                ctx,
                typeof uid === "string" && uid.length > 0,
                "Uid should not be empty!"
            );
            ctxAssert(
                ctx,
                typeof title === "string" && title.length > 0,
                "Title should not be empty!"
            );
        } catch (_) {
            return;
        }

        const decksColl = ctx.state.db.collection<Deck>("decks");
        const existingDeck = await decksColl.findOne({
            uid,
            author: user.nickname,
        });
        if (!existingDeck) {
            ctx.response.status = Number(process.env.STATUS_NOT_FOUND!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `Deck not found!`;
            return;
        }
        if (title === existingDeck.title) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `Duplicate title!`;
            return;
        }

        const result = await decksColl.updateOne(
            {
                uid,
                author: user.nickname,
            },
            {
                $set: {
                    title,
                }
            }
        );

        if (result.result.ok) {
            ctx.response.status = Number(process.env.STATUS_OK!);
            ctx.response.headers["content-type"] = "application/json";
            ctx.response.body = {
                success: true,
            };
        } else {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "DB temporary unavailable";
        }
    });

    router.post("/remove", async (ctx) => {
        let user: User;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        const uid = ctx.request.body?.uid;
        try {
            ctxAssert(
                ctx,
                typeof uid === "string" && uid.length > 0,
                "Uid should not be empty!"
            );
        } catch (_) {
            return;
        }

        const decksColl = ctx.state.db.collection<Deck>("decks");
        const existingDeck = await decksColl.findOne({
            uid,
            author: user.nickname,
        });
        if (!existingDeck) {
            ctx.response.status = Number(process.env.STATUS_NOT_FOUND!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `Deck not found!`;
            return;
        }
        const result1 = await decksColl.deleteOne({
            uid,
            author: user.nickname
        });
        if (!result1.result.ok) {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "Could not delete deck. DB temporary unavailable";
            return;
        }

        const cardsColl = ctx.state.db.collection<Card>("cards");
        const result2 = await cardsColl.deleteMany({
            deckUid: uid,
            author: user.nickname,
        });
        if (!result2.result.ok) {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "Could not delete related cards. DB temporary unavailable";
            return;
        }

        const templatesColl = ctx.state.templatesStorage.collection<CardTemplate>("templates");
        const result3 = await templatesColl.deleteMany({
            deckUid: uid,
            author: user.nickname,
        });
        if (!result3.result.ok) {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "Could not delete related templates. DB temporary unavailable";
            return;
        }

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers["content-type"] = "application/json";
        ctx.response.body = {
            success: true,
        };
    });

    return router.toHook();
}
