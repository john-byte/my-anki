import { Router } from "../../router";
import { State, AnkiCardRef, HookContext, User, Deck, Card, CardTemplate } from "../../types";
import fs from "fs";
import path from "path";
import { supermemo } from "../../utils/supermemo";
import { requireAuth } from "../../utils/require-auth";

enum CardSide {
    Front,
    Back
}

interface AnkiStateDict { 
    [name: string]: AnkiCardRef 
}

const DAY_IN_MS = 1000 * 60 * 60 * 24;

const isCardReadyToMemo = (stats: AnkiCardRef, now: Date): boolean => {
    if (typeof stats.passedAt === "undefined") {
        return true;
    }

    const passedAtMs = stats.passedAt.getTime();
    const willReadyMs = passedAtMs + (stats.interval * DAY_IN_MS);
    const nowMs = now.getTime();

    return nowMs >= willReadyMs;
};

const readCard = async (
    ctx: HookContext<State>,
    user: User,
) => {
    const body = ctx.request.body;
    const deckName = body.deck_name;
    const cardName = body.card_name;

    const templatesDb = ctx.state.templatesStorage;
    const templates = await templatesDb.collection<CardTemplate>("templates")
        .findOne({
            uid: cardName,
            deckUid: deckName,
            author: user.nickname,
        });

    return templates ? {
        front: templates.frontSide,
        back: templates.backSide,
    } : null;
}

const prepareTransactions = (
    body: any, 
    ankiState: AnkiStateDict,
    now: Date,
    user: User,
) => {
    const transactions = [];
    const nowStr = now.toISOString();

    for (let cardName in body.cards) {
        const cardData = ankiState[cardName];
        const nextAnkiState = supermemo(
            {
                interval: cardData.interval,
                repetition: cardData.repetition,
                efactor: cardData.efactor
            },
            body.cards[cardName]
        );

        transactions.push({
            updateOne: {
                filter: {
                    uid: cardName,
                    author: user.nickname,
                    deckUid: body.deck_name
                },
                update: {
                    $set: {
                        ...nextAnkiState,
                        passedAt: nowStr
                    }
                }
            }
        })
    }

    return transactions;
}

export const createAnkiRoutes = () => {
    const router = new Router<State>("/anki");

    router.get("/decks", async (ctx) => {
        let user: User | undefined;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        const decks = await ctx.state.db.collection<Deck>("decks")
            .find({
                author: user.nickname,
            })
            .toArray();

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers = {
            ...ctx.response.headers,
            "content-type": "application/json"
        };
        ctx.response.body = decks;
    });

    router.post("/deck-cards", async (ctx) => {
        let user: User | undefined;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        const body = ctx.request.body;

        if (typeof body !== "object" || typeof body.name !== "string") {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.body = "Invalid request body of /deck-cards";
            return;
        }

        const now = new Date();
        const deckCards = await ctx.state.db.collection<Card>("cards")
            .find(
                {
                    author: user.nickname,
                    deckUid: body.name
                }
            )
            .toArray();

        const cards = body.supermemoOn ? deckCards.filter(
            card => isCardReadyToMemo(
                {
                    ...card,
                    passedAt: card.passedAt ? new Date(card.passedAt) : undefined
                },
                now
            )
        ) : deckCards;

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers = {
            ...ctx.response.headers,
            "content-type": "application/json",
        };
        ctx.response.body = cards;
    });

    router.post("/card", async (ctx) => {
        let user: User | undefined;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        if (
            (typeof ctx.request.body !== "object") ||
            (typeof ctx.request.body.deck_name !== "string") ||
            (typeof ctx.request.body.card_name !== "string")
        ) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.body = "Invalid request body of /card";
            return;
        }

        const templates = await readCard(ctx, user);
        if (!templates) {
            ctx.response.status = Number(process.env.STATUS_NOT_FOUND);
            ctx.response.headers['content-type'] = "text/plain";
            ctx.response.body = `Card not found`;
            return;
        }

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers = {
            ...ctx.response.headers,
                "content-type": "application/json"
        };
        ctx.response.body = templates;
    });

    router.post("/update-scores", async (ctx) => {
        let user: User | undefined;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        if (
            (typeof ctx.request.body !== "object") ||
            (typeof ctx.request.body.deck_name !== "string") ||
            (typeof ctx.request.body.cards !== "object")
        ) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.body = "Invalid request body of /update-scores";
            return;
        }

        const db = ctx.state.db;
        const body = ctx.request.body;

        const ankiStates: AnkiStateDict = {};
        await db.collection<Card>("cards")
            .find({
                author: user.nickname,
                deckUid: body.deck_name
            })
            .forEach(card => {
                ankiStates[card.uid] = {
                    interval: card.interval,
                    repetition: card.repetition,
                    efactor: card.efactor,
                    // @ts-ignore Type 'string | null' is not assignable to type 'Date | undefined'
                    passedAt: card.passedAt
                }
            });

        const transactions = prepareTransactions(body, ankiStates, new Date(), user);
        await db.collection<Card>("cards").bulkWrite(transactions);

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers = {
            ...ctx.response.headers,
            "content-type": "application/json"
        };
        ctx.response.body = {
            success: true
        };
    });

    return router.toHook();
}
