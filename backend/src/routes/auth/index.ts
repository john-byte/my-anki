import crypto from "crypto";
import { Router } from "../../router";
import { State, User, PassAlgorithm, UserSession, MailerMessage, PassRecoverageSession } from "../../types";
import * as arrayUtils from "../../utils/byte-array";
import { ctxAssert } from "../../utils/ctx-assert";

const randomString = (length: number) => {
    const res = new Uint8Array(length);

    for (let i = 0; i < length; i++) {
        res[i] = (Math.random() * (125 - 33) + 33) ^ 0;
    }

    return res;
}

const randomUrlString = (length: number) => {
    const res: string[] = new Array(length);

    for (let i = 0; i < length; i++) {
        const kind = (Math.random() * 3) ^ 0;

        let code = 0;
        switch (kind) {
            case 0:
                code = (Math.random() * (58 - 48) + 48) ^ 0;
                break;
            case 1:
                code = (Math.random() * (91 - 65) + 65) ^ 0;
                break;
            case 2:
                code = (Math.random() * (123 - 97) + 97) ^ 0;
                break;
        }

        res[i] = String.fromCharCode(code);
    }

    return res.join("");
};

const getPassHash = (passAlgo: PassAlgorithm, password: Uint8Array, passSalt: Uint8Array) => {
    let raw: Uint8Array;
    switch (passAlgo) {
        case PassAlgorithm.Concat:
            raw = arrayUtils.concat(password, passSalt);
            break;
        case PassAlgorithm.Merge:
            raw = arrayUtils.merge(password, passSalt);
            break;
        case PassAlgorithm.ReverseAndConcat:
            raw = arrayUtils.concat(arrayUtils.reverse(password), passSalt);
            break;
        case PassAlgorithm.ReverseAndMerge:
            raw = arrayUtils.merge(arrayUtils.reverse(password), passSalt);
            break;
    }

    return crypto.createHash("sha256").update(raw).digest("hex");
}

const getPassDetails = (password: string) => {
    const passAlgo: PassAlgorithm = (Math.random() * 4) ^ 0;
    const passSalt = randomString(128);
    const passByteArr = new TextEncoder().encode(password);
    const passHash = getPassHash(passAlgo, passByteArr, passSalt);

    return {
        hash: passHash,
        salt: new TextDecoder().decode(passSalt),
        algorithm: passAlgo,
    };
};

const getConfirmLink = (confirmationCode: string) => {
    const baseUrl = `${process.env.BACKEND_BASE_URL}/auth/confirm-email`;
    const redirectBase = process.env.FRONTEND_BASE_URL!;

    const url = `${baseUrl}?confirm=${encodeURIComponent(confirmationCode)}&redirect=${encodeURIComponent(redirectBase)}`;
    return url;
};

const getSessCode = (user: User) => {
    const sessTimestamp = new Date().getTime();
    const sessSalt = new TextDecoder().decode(randomString(128));
    const sessCode = `${user.nickname}__${sessTimestamp}__${sessSalt}`;
    return sessCode;
};

const getPassRecoveryLink = (code: string) => {
    const baseUrl = `${process.env.FRONTEND_BASE_URL}/new-password`;
    const url = `${baseUrl}?code=${encodeURIComponent(code)}`;
    return url;
};

export const createAuthRoutes = () => {
    const router = new Router<State>("/auth");
    const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    router.post("/register", async (ctx) => {
        const username = ctx.request.body?.username;
        const email = ctx.request.body?.email;
        const password = ctx.request.body?.password;

        try {
            ctxAssert(ctx, typeof username === "string", "Invalid username");
            ctxAssert(ctx, typeof email === "string", "Invalid email");
            ctxAssert(ctx, typeof password === "string", "Invalid password");
            ctxAssert(ctx, username.length >= 3, "Username must be at least 3 characters long");
            ctxAssert(ctx, username.length <= 16, "Username must be at most 16 characters long");
            ctxAssert(ctx, emailRegex.test(email), "Invalid email");
            ctxAssert(ctx, password.length >= 8, "Password must be at least 8 characters long");
        } catch (_) {
            return;
        }

        const usersColl = ctx.state.db.collection<User>("users");
        
        const existingUserWithNickname = await usersColl.findOne({
            nickname: username,
        });
        if (existingUserWithNickname) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `User with this nickname already registered`;
            return;
        }

        const existingUserWithEmail = await usersColl.findOne({
            email,
        });
        if (existingUserWithEmail) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `User with this email already registered`;
            return;
        }

        const passDetails = getPassDetails(password);
        const confirmCodeSalt = randomUrlString(128);
        const confirmationCode = username + "__" + confirmCodeSalt;
        const result = await usersColl.insertOne({
            nickname: username,
            passwordConfig: passDetails,
            email,
            confirmed: false,
            confirmationCode,
        });

        if (result.result.ok) {
            ctx.response.status = Number(process.env.STATUS_OK!);
            ctx.response.headers["content-type"] = "application/json";
            ctx.response.body = {
                success: true,
            };

            const mailer = ctx.state.mailer;
            mailer.postMessage({
                action: "send",
                payload: {
                    email,
                    message: `Here's your confirmation link => ${getConfirmLink(confirmationCode)}`,
                }
            } as MailerMessage);
        } else {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "DB temporary unavailable";
        }
    });

    router.post("/login", async (ctx) => {
        const username = ctx.request.body?.username;
        const email = ctx.request.body?.email;
        const password = ctx.request.body?.password;

        try {
            ctxAssert(
                ctx, 
                typeof username === "string" || (typeof email === "string" && emailRegex.test(email)), 
                "Invalid username or email"
            );
            ctxAssert(ctx, typeof password === "string", "Invalid password");
        } catch (_) {
            return;
        }

        const usersColl = ctx.state.db.collection<User>("users");
        const existingUser = await usersColl.findOne({
            $or: [
                {
                    nickname: username,
                },
                {
                    email: email,
                }
            ]
        });

        if (!existingUser) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `User ${username} doesn't exist`;
            return;
        }

        const passHash = existingUser.passwordConfig.hash;
        const passSalt = existingUser.passwordConfig.salt;
        const passAlgo = existingUser.passwordConfig.algorithm;
        const computedPassHash = getPassHash(
            passAlgo, 
            new TextEncoder().encode(password),
            new TextEncoder().encode(passSalt),
        );

        if (computedPassHash !== passHash) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `Invalid password`;
            return;
        }

        const sessions = ctx.state.sessionsStorage.collection<UserSession>("sessions");
        const sessCode = getSessCode(existingUser);
        const result = await sessions.insertOne(
            {
                nickname: existingUser.nickname,
                sessionCode: sessCode,
            }
        );

        if (result.result.ok) {
            ctx.response.status = Number(process.env.STATUS_OK!);
            ctx.response.headers["content-type"] = "application/json";
            ctx.response.body = {
                success: true,
                sessCode,
            };
        } else {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "Session storage temporary unavailable";
        }
    });

    router.get("/confirm-email", async (ctx) => {
        const confirmationCode = ctx.request.query.confirm;
        if (typeof confirmationCode !== "string" || confirmationCode.length === 0) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.headers = {
                ...ctx.response.headers,
                "content-type": "text/plain; charset=utf-8"
            };
            ctx.response.body = "Bad confirmation code";
            return;
        }

        const encodedRedirect = ctx.request.query.redirect;
        if (typeof encodedRedirect !== "string" || encodedRedirect.length === 0) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.headers = {
                ...ctx.response.headers,
                "content-type": "text/plain; charset=utf-8"
            };
            ctx.response.body = "Bad redirect link";
            return;
        }
        const redirect = decodeURIComponent(encodedRedirect);

        const users = ctx.state.db.collection<User>("users");
        const user = await users.findOne(
            {
                confirmationCode
            }
        );

        if (!user) {
            ctx.response.status = Number(process.env.STATUS_NOT_FOUND!);
            ctx.response.headers = {
                ...ctx.response.headers,
                "content-type": "text/plain; charset=utf-8"
            };
            ctx.response.body = "User with this confirmation code not found";
            return;
        }

        if (user.confirmed) {
            ctx.response.status = Number(process.env.STATUS_MOVED!);
            ctx.response.headers = {
                ...ctx.response.headers,
                "content-type": "text/plain; charset=utf-8",
            };
            // TODO: add convinient "already confirmed" content
            ctx.response.body = "You are already confirmed email";
            return;
        }

        await users.updateOne(
            {
                confirmationCode
            },
            {
                $set: {
                    confirmed: true
                }
            }
        );

        // TODO: move redirect to context class or special function
        ctx.response.status = Number(process.env.STATUS_MOVED!);
        // TODO: incapsulate context fields into class
        ctx.response.headers = {
            ...ctx.response.headers,
            "content-type": "text/plain; charset=utf-8",
            "Location": redirect,
        };
        ctx.response.body = "";
    });

    router.post("/send-pass-recover", async (ctx) => {
        const username = ctx.request.body?.username;
        const email = ctx.request.body?.email;

        try {
            const isValidNick = typeof username === "string" && !!username.length;
            const isValidEmail = typeof email === "string" && emailRegex.test(email);
            ctxAssert(ctx, isValidNick || isValidEmail, "Invalid username or email");
        } catch (_) {
            return;
        }

        const db = ctx.state.db;
        const user = await db.collection<User>("users").findOne({
            $or: [
                {
                    nickname: username,
                },
                {
                    email,
                }
            ]
        });

        if (!user) {
            ctx.response.status = Number(process.env.STATUS_NOT_FOUND!);
            ctx.response.headers = {
                ...ctx.response.headers,
                "content-type": "text/plain"
            };
            ctx.response.body = `User ${username} (${email}) not found`;
            return;
        }

        if (!user.confirmed) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST);
            ctx.response.headers = {
                ...ctx.response.headers,
                "content-type": "text/plain"
            };
            ctx.response.body = `User ${user.nickname} didn't confirm email`;
            return;
        }

        const sessStorage = ctx.state.sessionsStorage;
        const passRecovSessions = sessStorage.collection<PassRecoverageSession>("pass-recoverage");
        const sessCode = getSessCode(user);

        const result = await passRecovSessions.insertOne({
            nickname: user.nickname,
            sessionCode: sessCode,
        });

        if (result.result.ok) {
            ctx.response.status = Number(process.env.STATUS_OK!);
            ctx.response.headers["content-type"] = "application/json";
            ctx.response.body = {
                success: true,
            };

            ctx.state.mailer.postMessage(
                {
                    action: "send",
                    payload: {
                        email: user.email,
                        message: `Here's your password recovery link => ${getPassRecoveryLink(sessCode)}`
                    }
                } as MailerMessage
            );
        } else {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "Session storage temporary unavailable";
        }
    });

    router.post("/recover-password", async (ctx) => {
        const code = ctx.request.body?.code;
        const newPassword = ctx.request.body?.newPassword;

        try {
            ctxAssert(ctx, typeof code === "string" && !!code, "Invalid recovery code");
            ctxAssert(ctx, typeof newPassword === "string", "Invalid password");
            ctxAssert(ctx, newPassword.length >= 8, "Password must be at least 8 characters long");
        } catch (e) {
            return;
        }

        const sessStorage = ctx.state.sessionsStorage;
        const session = await sessStorage.collection<PassRecoverageSession>("pass-recoverage").findOne({
            sessionCode: code,
        });

        if (!session) {
            ctx.response.status = Number(process.env.STATUS_NOT_FOUND!);
            ctx.response.headers = {
                ...ctx.response.headers,
                "content-type": "text/plain"
            };
            ctx.response.body = `Session not found`;
            return;
        }

        const passDetails = getPassDetails(newPassword);
        const db = ctx.state.db;
        
        const result = await db.collection<User>("users").updateOne(
            {
                nickname: session.nickname,
            },
            {
                $set: {
                    passwordConfig: passDetails
                },
            }
        );

        if (!result.result.ok) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `Cannot update password for ${session.nickname}`;
            return;
        }

        const resultCleaning = await sessStorage.collection<PassRecoverageSession>("pass-recoverage")
            .deleteMany({
                nickname: session.nickname,
            });

        if (!resultCleaning.result.ok) {
            ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `Cannot delete pass recovery session for ${session.nickname}`;
            return;
        }

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers["content-type"] = "application/json";
        ctx.response.body = {
            success: true,
        };
    });

    return router.toHook();
}
