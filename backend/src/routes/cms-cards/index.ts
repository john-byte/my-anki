import { Router } from "../../router";
import { State, User, Deck, Card, CardTemplate } from "../../types";
import { ctxAssert } from "../../utils/ctx-assert";
import { requireAuth } from "../../utils/require-auth";

export const createCmsCardsRoutes = () => {
    const router = new Router<State>("/cms/cards");

    router.get("/all", async (ctx) => {
        let user: User;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        const deckUid = ctx.request.query['deck'];
        try {
            ctxAssert(
                ctx,
                typeof deckUid === "string" && deckUid.length > 0,
                "Deck uid should not be empty!"
            );
        } catch (_) {
            return;
        }

        const db = ctx.state.db;
        const cards = await db.collection<Card>("cards")
            .find({
                deckUid: deckUid as string,
                author: user.nickname,
            })
            .toArray();

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers["content-type"] = "application/json";
        ctx.response.body = cards;
    });

    router.post("/add", async (ctx) => {
        let user: User;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        const deckUid = ctx.request.body?.deckUid;
        let title = ctx.request.body?.title;
        if (typeof title === "string") {
            title = title.trim();
        }
        const frontSide = ctx.request.body?.frontSide;
        const backSide = ctx.request.body?.backSide;
        try {
            ctxAssert(
                ctx,
                typeof deckUid === "string" && deckUid.length > 0,
                "Deck uid should not be empty!"
            );
            ctxAssert(
                ctx,
                typeof title === "string" && title.length > 0,
                "Title should not be empty!"
            );
            ctxAssert(
                ctx,
                typeof frontSide === "string" && frontSide.length > 0,
                "Front side should not be empty!"
            );
            ctxAssert(
                ctx,
                typeof backSide === "string" && backSide.length > 0,
                "Back side should not be empty!"
            );
        } catch (_) {
            return;
        }

        const db = ctx.state.db;
        const deck = await db.collection<Deck>("decks")
            .findOne({
                uid: deckUid,
                author: user.nickname,
            });
        if (!deck) {
            ctx.response.status = Number(process.env.STATUS_FORBIDDEN!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `User ${user.nickname} has no deck with uid "${deckUid}"`;
            return;
        } 

        const uid = `${user.nickname}__${(new Date()).getTime()}`;
        let resultOfMeta = await db.collection<Card>("cards")
            .insertOne({
                uid,
                deckUid,
                title,
                author: user.nickname,
                repetition: 0,
                interval: 0,
                efactor: 2.5,
                passedAt: null,
            });
        if (!resultOfMeta.result.ok) {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "DB temporary unavailable";
            return;
        }

        const templatesDb = ctx.state.templatesStorage;
        let resultOfTemplate = await templatesDb.collection<CardTemplate>("templates")
            .insertOne({
                uid,
                deckUid,
                author: user.nickname,
                frontSide,
                backSide,
            });
        if (!resultOfTemplate.result.ok) {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "DB temporary unavailable";
            return;
        }

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers["content-type"] = "application/json";
        ctx.response.body = {
            success: true,
        };
    });

    router.post("/edit", async (ctx) => {
        let user: User;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        const uid = ctx.request.body?.uid;
        const deckUid = ctx.request.body?.deckUid;
        let title = ctx.request.body?.title;
        if (typeof title === "string") {
            title = title.trim();
        }

        const frontSide = ctx.request.body?.frontSide;
        const backSide = ctx.request.body?.backSide;
        const isTitlePresent = typeof title === "string" && title.length > 0;
        const isFrontSidePresent = typeof frontSide === "string" && frontSide.length > 0;
        const isBackSidePresent = typeof backSide === "string" && backSide.length > 0;
        try {
            ctxAssert(
                ctx,
                typeof uid === "string" && uid.length > 0,
                "Card uid should not be empty!"
            );
            ctxAssert(
                ctx,
                typeof deckUid === "string" && deckUid.length > 0,
                "Deck uid should not be empty!"
            );
            ctxAssert(
                ctx,
                isTitlePresent || isFrontSidePresent || isBackSidePresent,
                "Title, frontSize or backSide at least should not be empty!"
            );
        } catch (_) {
            return;
        }

        const db = ctx.state.db;
        const deck = await db.collection<Deck>("decks")
            .findOne({
                uid: deckUid,
                author: user.nickname,
            });
        if (!deck) {
            ctx.response.status = Number(process.env.STATUS_FORBIDDEN!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `User ${user.nickname} has no deck with uid "${deckUid}"`;
            return;
        }
        
        const card = await db.collection<Card>("cards")
            .findOne({
                uid,
                deckUid,
                author: user.nickname,
            });
        if (!card) {
            ctx.response.status = Number(process.env.STATUS_FORBIDDEN!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `User ${user.nickname} has no card with uid "${uid}"`;
            return;
        }
        if (isTitlePresent) {
            let resultOfMeta = await db.collection<Card>("cards")
                .updateOne(
                    {
                        uid,
                        deckUid,
                        author: user.nickname,
                    },
                    {
                        title,
                    }
                );
            if (!resultOfMeta.result.ok) {
                ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
                ctx.response.headers["content-type"] = "text/plain";
                ctx.response.body = "DB temporary unavailable";
                return;
            }
        }

        const templatesDb = ctx.state.templatesStorage;
        const updateQuery: any = {};
        if (isFrontSidePresent) {
            updateQuery.frontSide = frontSide;
        }
        if (isBackSidePresent) {
            updateQuery.backSide = backSide;
        }
        if (isFrontSidePresent || isBackSidePresent) {
            let resultOfTemplate = await templatesDb.collection<CardTemplate>("templates")
                .updateOne(
                    {
                        uid,
                        author: user.nickname,
                    },
                    {
                        $set: updateQuery
                    }
                );
            if (!resultOfTemplate.result.ok) {
                ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
                ctx.response.headers["content-type"] = "text/plain";
                ctx.response.body = "DB temporary unavailable";
                return;
            }
        }

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers["content-type"] = "application/json";
        ctx.response.body = {
            success: true,
        };
    });

    router.post("/remove", async (ctx) => {
        let user: User;
        try {
            user = requireAuth(ctx);
        } catch (_) {
            return;
        }

        const uid = ctx.request.body?.uid;
        try {
            ctxAssert(
                ctx,
                typeof uid === "string" && uid.length > 0,
                "Uid should not be empty!"
            );
        } catch (_) {
            return;
        }

        const cardsColl = ctx.state.db.collection<Card>("cards");
        const existingCard = await cardsColl.findOne({
            uid,
            author: user.nickname,
        });
        if (!existingCard) {
            ctx.response.status = Number(process.env.STATUS_NOT_FOUND!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = `Card not found!`;
            return;
        }
        const result1 = await cardsColl.deleteOne({
            uid,
            author: user.nickname
        });
        if (!result1.result.ok) {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "DB temporary unavailable";
            return;
        }

        const templatesDb = ctx.state.templatesStorage;
        const templatesColl = templatesDb.collection<CardTemplate>("templates");
        const result2 = await templatesColl.deleteOne({
            uid: uid,
            author: user.nickname,
        });
        if (!result2.result.ok) {
            ctx.response.status = Number(process.env.STATUS_SERVER_ERROR!);
            ctx.response.headers["content-type"] = "text/plain";
            ctx.response.body = "DB temporary unavailable";
            return;
        }

        ctx.response.status = Number(process.env.STATUS_OK!);
        ctx.response.headers["content-type"] = "application/json";
        ctx.response.body = {
            success: true,
        };
    });

    return router.toHook();
}
