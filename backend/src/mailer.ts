import nodemailer from "nodemailer";
import { parentPort } from "worker_threads";
import { MailerMessage } from "./types";

const { config } = require("./nodemailer-cfg.js");

const main = async () => {
    const mailer = nodemailer.createTransport(config);

    parentPort?.on("message", async (msg: MailerMessage) => {
        const toEmail = msg.payload.email;
        const message = msg.payload.message;

        try {
            await mailer.sendMail({
                from: config.auth.user,
                to: toEmail,
                subject: "Confirm your email",
                // TODO: add reliable template
                text: `${message}`,
                html: `${message}`,
            });
        } catch (e) {
            console.error(e);
        }
    });

    console.log("[MAILER] Started");
}

main().catch(e => console.error("[MAILER] "+ e));
