const slugDelim = /[\s\-_]/;
export const slugify = (s: string) => {
    const res: string[] = [];

    let ptr = 0;
    while (ptr < s.length) {
        if (slugDelim.test(s[ptr])) {
            const oldPtr = ptr;
            
            do {
                ptr++;
            } while (ptr < s.length && slugDelim.test(s[ptr]));

            if (oldPtr > 0 && ptr < s.length) {
                res.push("-");
            }

            continue;
        }

        if (("a" <= s[ptr] && s[ptr] <= "z") || ("A" <= s[ptr] && s[ptr] <= "Z")) {
            res.push(s[ptr].toLowerCase());
            ptr++;
            continue;
        }

        if (true) {
            res.push(s[ptr]);
            ptr++;
            continue;
        }
    }

    return res.join("");
}