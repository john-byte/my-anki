import { HookContext, State } from "../types";

export const requireAuth = (ctx: HookContext<State>) => {
    const user = ctx.state.user;
    if (!user) {
        ctx.response.status = Number(process.env.STATUS_FORBIDDEN!);
        ctx.response.headers["content-type"] = "text/plain";
        ctx.response.body = `Not authorized`;
        throw "Not authorized";
    }

    return user;
};