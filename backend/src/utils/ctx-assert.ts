import { HookContext, State } from "../types";

export const ctxAssert = (ctx: HookContext<State>, condition: boolean, errMessage: string) => {
    if (!condition) {
        ctx.response.status = Number(process.env.STATUS_BAD_REQUEST!);
        ctx.response.headers = {
            ...ctx.response.headers,
            "Content-Type": "text/plain"
        };
        ctx.response.body = errMessage;
        throw "Assertion failed";
    }
}