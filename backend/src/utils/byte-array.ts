export const concat = (s1: Uint8Array, s2: Uint8Array) => {
    const res = new Uint8Array(s1.length + s2.length);

    let ptr = 0;
    for (; ptr < s1.length; ptr++) {
        res[ptr] = s1[ptr];
    }

    for (; ptr < s2.length; ptr++) {
        res[ptr] = s2[ptr];
    }

    return res;
}

export const merge = (s1: Uint8Array, s2: Uint8Array) => {
    const res = new Uint8Array(s1.length + s2.length);

    const minLen = Math.min(s1.length, s2.length);
    let ptr = 0;
    for (; ptr < minLen; ptr++) {
        res[ptr * 2] = s1[ptr];
        res[ptr * 2 + 1] = s2[ptr];
    }

    const remStr = minLen === s1.length ? s2 : s1;
    for (; ptr < remStr.length; ptr++) {
        res[ptr + minLen] = remStr[ptr];
    }

    return res;
}

export const reverse = (s: Uint8Array) => s.reverse();
