import { useState } from "react";
import { Dict } from "../types";

type Validator = (v: string, fields?: Dict<string>) => boolean;

interface UseFormParams {
    fields: {
        [field: string]: {
            validate: Validator;
            errorMessage: string;
        }
    };
    onSubmit: (fields: Dict<string>) => Promise<any>;
    onFieldChange?: (field?: string, v?: string) => void;
}
export const useForm = ({ fields: fieldsDict, onSubmit, onFieldChange }: UseFormParams) => {
    const initFields = () => {
        const res: Dict<string> = {};
        for (const f in fieldsDict) {
            res[f] = '';
        }
        return res;
    };

    const [fields, setFields] = useState<Dict<string>>(initFields);
    const [errors, setErrors] = useState<Dict<string>>(initFields);

    const getValue = (field: string) => fields[field];
    const setValue = (field: string, val: string) => {
        if (typeof fields[field] === "undefined") {
            throw `Field ${field} doesn't exist`;
        }

        setFields((p) => ({ ...p, [field]: val }));
        setErrors((p) => ({ ...p, [field]: '' }));

        if (onFieldChange) {
            onFieldChange(field, val);
        }
    }

    const getError = (field: string) => errors[field];

    const submit = async () => {
        let hasError = false;

        for (const f in fieldsDict) {
            if (!fieldsDict[f].validate(fields[f], fields)) {
                setErrors((p) => ({ ...p, [f]: fieldsDict[f].errorMessage }));
                hasError = true;
            }
        }

        if (hasError) return;

        await onSubmit(fields);
    }

    return { getValue, setValue, getError, submit }; 
}
