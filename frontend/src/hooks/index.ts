import { useState, useEffect } from "react";

export { useForm } from "./form";
export const useForceUpdate = () => {
    const [flag, setFlag] = useState(false);
    return () => setFlag(!flag);
}

export const useAsyncMemo = <T>(fn: (abortSig?: AbortSignal) => Promise<T>) => {
    const [pending, setPending] = useState(true);
    const [error, setError] = useState<Error>();
    const [data, setData] = useState<T>();

    const [refetchSig, setRefetchSig] = useState(false);
    const refetch = () => setRefetchSig((p) => !p);

    useEffect(() => {
        const aborter = new AbortController();
        setPending(true);

        fn(aborter.signal)
            .then(res => {
                setData(res);
            })
            .catch(err => {
                if (err?.name !== "AbortError") {
                    console.error(err);
                    setError(err);
                }
            })
            .finally(() => {
                setPending(false);
            });
            
        return () => {
            aborter.abort();
        }
    }, [fn, refetchSig]);

    return {
        pending,
        error,
        data,
        refetch,
    };
}
