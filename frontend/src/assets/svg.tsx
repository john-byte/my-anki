import styled from "styled-components";

export const Svg = styled.svg`
    height: 100%;
    width: auto;
`;