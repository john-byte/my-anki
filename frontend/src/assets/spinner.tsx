import React from "react";
import styled, { keyframes } from "styled-components";
import { Svg as RawSvg } from "./svg";

const rotation = keyframes`
    0% {
        transform: rotate(0deg);
    }

    50% {
        transform: rotate(180deg);
    }

    100% {
        transform: rotate(0deg);
    }
`;

const Svg = styled(RawSvg)`
    animation-name: ${rotation};
    animation-duration: 2s;
    animation-iteration-count: infinite;
`;


export const Spinner = ({ color, className }: { color?: string; className?: string; }) => {
    return <Svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="circle-notch" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" className={className}><path fill={color} d="M288 28.977v16.391c0 7.477 5.182 13.945 12.474 15.598C389.568 81.162 456 160.742 456 256c0 110.532-89.451 200-200 200-110.532 0-200-89.451-200-200 0-95.244 66.422-174.837 155.526-195.034C218.818 59.313 224 52.845 224 45.368V28.981c0-10.141-9.322-17.76-19.246-15.675C91.959 37.004 7.373 137.345 8.004 257.332c.72 137.052 111.477 246.956 248.531 246.667C393.255 503.711 504 392.788 504 256c0-119.349-84.308-219.003-196.617-242.665C297.403 11.232 288 18.779 288 28.977z"></path></Svg>
}
