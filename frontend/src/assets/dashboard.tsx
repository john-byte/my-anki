import React from "react";

export const Dashboard = ({ color, className }: { color?: string; className?: string; }) => {
    return (
        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="columns" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" className={className}>
            <path fill={color} d="M464 32H48C21.49 32 0 53.49 0 80v352c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48V80c0-26.51-21.49-48-48-48zM232 432H54a6 6 0 0 1-6-6V112h184v320zm226 0H280V112h184v314a6 6 0 0 1-6 6z">
            </path>
        </svg>
    )
}
