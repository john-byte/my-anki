import { Dict } from "../types";

export const makeFormSubmit = (
    url: string,
    formFieldsFilter: (fields: Dict<string>) => Dict<string>,
    onSuccess: (responseData?: any) => void, 
    onError: (e?: Error | string) => void,
    validateResponseData: (data: any) => boolean, 
    validationError: string | Error,
    begin?: () => void,
    end?: () => void,
) => async (fields: Dict<string>) => {
    try {
        if (begin) {
            begin();
        }

        const resp = await fetch(
            url,
            {
                method: "POST",
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify(
                    formFieldsFilter(fields)
                ),
            },
        );

        if (resp.status !== 200) {
            const text = await resp.text();
            throw text;
        }

        const resData = await resp.json();
        if (!validateResponseData(resData)) {
            throw validationError;
        }

        onSuccess(resData);
    } catch (e) {
        // @ts-ignore
        console.error(e);
        onError(e as any);
    } finally {
        if (end) {
            end();
        }
    }
}