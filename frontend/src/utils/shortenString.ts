export const shortenString = (s: string, maxCharsCnt: number, replSec = "..."): string => {
    const availableLen = maxCharsCnt - replSec.length;
    if (s.length > availableLen) {
        return s.slice(0, availableLen) + replSec;
    } else {
        return s;
    }
};
