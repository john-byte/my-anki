import React from "react";
import { useHistory } from "react-router-dom";
import styles from "./index.module.css";
import { H1 } from "../common/Text";
import { Button } from "../common/Button";

const FinishScreen: React.FC = () => {
    const history = useHistory();

    return (
        <div className={styles.wrapper}>
            <H1>
                Come back to decks
            </H1>

            <div style={{ height: "20px" }}/>

            <div>
                <Button.Primary onClick={() => history.push("/")}>
                    Back
                </Button.Primary>
            </div>
        </div>
    )
}

export default FinishScreen;
