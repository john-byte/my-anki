import React from "react";
import { useHistory, useRouteMatch } from "react-router";
import styled from "styled-components";
import { applyTheme } from "../common/themes";
import { Logout } from "../../assets/logout";
import { Dashboard } from "../../assets/dashboard";
import { Home } from "../../assets/home";
import { Tooltip } from '../common/Tooltip';

const Wrapper = styled.header`
    position: sticky;
    top: 0;
    left: 0;
    width: 100%;
    height: 48px;
    box-sizing: border-box;
    background-color: ${props => applyTheme(props).colors.background};
    border: ${props => applyTheme(props).border.default};
    box-shadow: ${props => applyTheme(props).boxShadow.default};

    display: flex;
    justify-content: flex-end;
`;

const Item = styled.div`
    width: 48px;
    height: 100%;
    border-left: ${props => applyTheme(props).border.default};
    padding: 8px;
    cursor: pointer;
    box-sizing: border-box;

    background-color: ${props => applyTheme(props).colors.background};

    .icon {
        path {
            fill: ${props => applyTheme(props).colors.primary};
        }
    }

    &.active {
        .icon {
            path {
                fill: ${props => applyTheme(props).colors.secondary1};
            }
        }
    }

    &.filler {
        cursor: default;

        &:hover {
            background-color: ${props => applyTheme(props).colors.background};

            .icon {
                path {
                    fill: ${props => applyTheme(props).colors.primary};
                }
            }
        }
    }

    &:hover {
        background-color: ${props => applyTheme(props).colors.primary};

        .icon {
            path {
                fill: ${props => applyTheme(props).colors.background};
            }
        }
    }
`;

const Header = () => {
    const history = useHistory();
    
    const isDashboard = !!useRouteMatch("/dashboard");
    const isHome = !isDashboard;

    const logout = () => {
        localStorage.removeItem("sesscode");
        history.push("/login");
    };

    return (
        <Wrapper>
            <Tooltip down content={'Your quizes'}>
                <Item
                    className={isHome ? "active" : ""}
                    onClick={() => {
                        if (!isHome) {
                            history.push("/");
                        }
                    }}
                >
                    <Home className="icon" />
                </Item>
            </Tooltip>

            <Tooltip down content={'Dashboard'}>
                <Item 
                    className={isDashboard ? "active" : ""}
                    onClick={() => {
                        if (!isDashboard) {
                            history.push("/dashboard");
                        }
                    }}
                >
                    <Dashboard className="icon" />
                </Item>
            </Tooltip>

            <Item className="filler" style={{ width: "144px" }} />

            <Tooltip down content={'Logout'}>
                <Item onClick={logout}>
                    <Logout className="icon" />
                </Item>
            </Tooltip>
        </Wrapper>
    )
}

export default Header;
