import React, { useRef, useState } from "react";
import styled from "styled-components";
import { applyTheme } from "../themes";

const Wrapper = styled.div`
    position: relative;
`;

const Content = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    z-index: 2;
    min-width: 64px;
    min-height: 24px;
    padding: 4px;
    background-color: ${props => applyTheme(props).colors.background};
    color: ${props => applyTheme(props).colors.primary};
    border: ${props => applyTheme(props).border.default};
    box-shadow: ${props => applyTheme(props).boxShadow.default};

    font-family: ${props => applyTheme(props).fontFamilies.default};
    font-size: ${props => applyTheme(props).fontSizes.small};
    line-height: ${props => applyTheme(props).lineHeights.small};
`;

interface Props {
    content: React.ReactNode;
    children?: React.ReactNode;
    className?: string;
    down?: boolean
}

export const Tooltip = ({ content, className, down, children }: Props) => {
    const [contentPos, setContentPos] = useState<{ x: number; y: number; }>();

    const contentRef = useRef<HTMLElement>(null);
    const handleMouseEnter = (e: React.MouseEvent) => {
        const content = contentRef.current;
        if (content) {
            const style = window.getComputedStyle(content);
            const width = parseInt(style.width, 10);
            const height = parseInt(style.height, 10);

            const contentRect = content.getBoundingClientRect();
            const mouseX = e.clientX - contentRect.left;
            const mouseY = e.clientY - contentRect.top;

            const extraYPx = down ? 48 : -16;

            const moveX = -width / 2 + mouseX;
            const moveY = -height + mouseY + extraYPx;

            setContentPos({ x: moveX, y: moveY });
        }
    };

    const handleMouseLeave = () => {
        setContentPos(undefined);
    };

    const contentVisibility = contentPos ? 'visible' : 'hidden';
    const contentTransform = contentPos ? `translate(${contentPos.x}px, ${contentPos.y}px)` : '';
    
    return (
        <Wrapper className={className} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
            <Content
                // @ts-ignore 
                ref={contentRef}
                style={{ visibility: contentVisibility, transform: contentTransform }} 
            >
                {content}
            </Content>

            {children}
        </Wrapper>
    )
}
