import styled from "styled-components";
import { applyTheme } from "../themes";

interface ButtonProps {
    width?: string;
    height?: string;
}

const ButtonBase = styled.button<ButtonProps>`
    margin: 0;
    padding: 0;
    position: relative;
    box-sizing: border-box;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    min-width: ${props => props.width ?? "160px"};
    max-width: ${props => props.width ?? "200px"};
    min-height: ${props => props.height ?? "40px"};
    padding: 0 12px;

    font-family: ${props => applyTheme(props).fontFamilies.default};
    font-style: regular;
    font-weight: bold;
    font-size: ${props => applyTheme(props).fontSizes.regular};
    line-height: ${props => applyTheme(props).lineHeights.regular};

    &:disabled {
        cursor: not-allowed;
    }
`;

// TODO: add styles for disabled state
const Primary = styled(ButtonBase)`
    border: 1px solid transparent;
    background-color: ${props => applyTheme(props).colors.dark};
    color: ${props => applyTheme(props).colors.primary};

    &:hover:not(:disabled) {
        background-color: ${props => applyTheme(props).colors.primary};
        color: ${props => applyTheme(props).colors.background};
    }

    &:active:not(:disabled) {
        background-color: ${props => applyTheme(props).colors.background};
        color: ${props => applyTheme(props).colors.primary};
    }
`;

const Danger = styled(ButtonBase)`
    border: 1px solid transparent;
    background-color: ${props => applyTheme(props).colors.secondary1};
    color: ${props => applyTheme(props).colors.primary};

    &:hover:not(:disabled) {
        background-color: ${props => applyTheme(props).colors.primary};
        color: ${props => applyTheme(props).colors.background};
    }

    &:active:not(:disabled) {
        background-color: ${props => applyTheme(props).colors.background};
        color: ${props => applyTheme(props).colors.primary};
    }
`;

const Secondary = styled(ButtonBase)`
    border: 1px solid ${props => applyTheme(props).colors.primary};
    background-color: ${props => applyTheme(props).colors.ghost};
    color: ${props => applyTheme(props).colors.primary};

    &:hover:not(:disabled) {
        background-color: ${props => applyTheme(props).colors.primary};
        color: ${props => applyTheme(props).colors.background};
    }

    &:active:not(:disabled) {
        border: 1px solid transparent;
        background-color: ${props => applyTheme(props).colors.background};
        color: ${props => applyTheme(props).colors.primary};
    }
`;

const Link = styled(ButtonBase)`
    min-width: unset;
    max-width: unset;
    min-height: unset;
    padding: unset;

    border: unset;
    background-color: transparent;
    color: ${props => applyTheme(props).colors.primary};
    text-decoration: underline;

    &:hover:not(:disabled) {
        text-decoration: none;
    }
`;

export const Button = Object.assign({}, {
    Primary,
    Secondary,
    Danger,
    Link,
});
