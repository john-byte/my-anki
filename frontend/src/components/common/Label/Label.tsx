import styled from "styled-components";
import { applyTheme } from "../themes";

export const Label = styled.label`
    font-family: ${props => applyTheme(props).fontFamilies.default};
    font-size: ${props => applyTheme(props).fontSizes.small};
    line-height: ${props => applyTheme(props).lineHeights.small};
`;
