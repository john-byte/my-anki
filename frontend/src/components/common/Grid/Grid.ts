import { CSSProperties } from "react";
import styled from "styled-components";

interface RowProps {
    justify?: CSSProperties["justifyContent"];
    align?: CSSProperties["alignItems"];
    wrap?: CSSProperties["flexWrap"];
}

export const Row = styled.div<RowProps>`
    display: flex;
    justify-content: ${props => props.justify ?? "flex-start"};
    align-items: ${props => props.align ?? "flex-start"};
    flex-wrap: ${props => props.wrap ?? "wrap"};
`;

interface ColProps {
    width?: string;
    height?: string;
}

export const Col = styled.div<ColProps>`
    width: ${props => props.width};
    height: ${props => props.height};
`;

interface StackProps {
    justify?: CSSProperties["justifyContent"];
    align?: CSSProperties["alignItems"];
}

export const Stack = styled.div<StackProps>`
    display: flex;
    flex-direction: column;
    justify-content: ${props => props.justify ?? "flex-start"};
    align-items: ${props => props.align ?? "flex-start"};
`;
