export interface Theme {
    colors: {
        background: string;
        primary: string;
        secondary1: string;
        secondary2: string;
        dark: string;
        ghost: string;
    };
    border: {
        default: string;
    };
    boxShadow: {
        default: string;
    };
    fontFamilies: {
        default: string;
    };
    fontSizes: {
        extraLarge: string;
        large: string;
        medium: string;
        regular: string;
        small: string;
        extraSmall: string;
    };
    lineHeights: {
        extraLarge: string;
        large: string;
        medium: string;
        regular: string;
        small: string;
        extraSmall: string;
    };
};
