import { Theme } from "../types";

export const DefaultTheme: Theme = {
    colors: {
        background: "#0054aa",
        primary: "#fffcfc",
        secondary1: "#fb8805",
        secondary2: "#e2283e",
        dark: "#120104",
        ghost: "transparent",
    },
    border: {
        default: "2px solid #fffcfc",
    },
    boxShadow: {
        default: "5px 5px 0px -2px #120104",
    },
    fontFamilies: {
        default: "Mono",
    },
    fontSizes: {
        extraLarge: "48px",
        large: "32px",
        medium: "24px",
        regular: "16px",
        small: "12px",
        extraSmall: "10px",
    },
    lineHeights: {
        extraLarge: "56px",
        large: "40px",
        medium: "32px",
        regular: "24px",
        small: "20px",
        extraSmall: "18px",
    },
};
