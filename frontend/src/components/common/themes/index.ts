import { DefaultTheme } from "./DefaultTheme";

export const applyTheme = ({ theme = DefaultTheme }) => theme;

export { DefaultTheme };