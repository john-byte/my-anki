import styled from "styled-components";
import { applyTheme } from "../themes";

export const Card = styled.div`
    position: relative;
    box-sizing: border-box;
    width: 100%;
    height: 100%;
    padding: 12px;
    background-color: ${props => applyTheme(props).colors.background};
    border: ${props => applyTheme(props).border.default};
    box-shadow: ${props => applyTheme(props).boxShadow.default};
`;
