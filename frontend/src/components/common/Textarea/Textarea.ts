import styled from "styled-components";
import { applyTheme } from "../themes";

export const Textarea = styled.textarea`
    display: block;
    resize: none;
    box-sizing: border-box;
    width: 100%;
    min-height: 80px;
    background-color: ${props => applyTheme(props).colors.background};
    height: 24px;
    font-size: ${props => applyTheme(props).fontSizes.regular};
    line-height: ${props => applyTheme(props).lineHeights.regular};
    color: black;
    outline: none;
    border: ${props => applyTheme(props).border.default};
    padding-left: 8px;
`;