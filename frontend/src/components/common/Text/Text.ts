import { CSSProperties } from "react";
import styled from "styled-components";
import { applyTheme } from "../themes";

interface TextProps {
    fontStyle?: CSSProperties["fontStyle"];
}

export const H1 = styled.h1<TextProps>`
    margin: 0;
    padding: 0;
    font-family: ${props => applyTheme(props).fontFamilies.default};
    font-style: ${props => props.fontStyle ?? "regular"};
    font-weight: bold;
    font-size: ${props => applyTheme(props).fontSizes.extraLarge};
    line-height: ${props => applyTheme(props).lineHeights.extraLarge};
    color: ${props => applyTheme(props).colors.dark};
`;

export const H2 = styled.h2<TextProps>`
    margin: 0;
    padding: 0;
    font-family: ${props => applyTheme(props).fontFamilies.default};
    font-style: ${props => props.fontStyle ?? "regular"};
    font-weight: bold;
    font-size: ${props => applyTheme(props).fontSizes.large};
    line-height: ${props => applyTheme(props).lineHeights.large};
    color: ${props => applyTheme(props).colors.dark};
`;

export const H3 = styled.h3<TextProps>`
    margin: 0;
    padding: 0;
    font-family: ${props => applyTheme(props).fontFamilies.default};
    font-style: ${props => props.fontStyle ?? "regular"};
    font-weight: bold;
    font-size: ${props => applyTheme(props).fontSizes.medium};
    line-height: ${props => applyTheme(props).lineHeights.medium};
    color: ${props => applyTheme(props).colors.dark};
`;

export const P = styled.p<TextProps>`
    margin: 0;
    padding: 0;
    font-family: ${props => applyTheme(props).fontFamilies.default};
    font-style: ${props => props.fontStyle ?? "regular"};
    font-weight: regular;
    font-size: ${props => applyTheme(props).fontSizes.regular};
    line-height: ${props => applyTheme(props).lineHeights.regular};
    color: ${props => applyTheme(props).colors.dark};
`;

export const PSmall = styled.p<TextProps>`
    margin: 0;
    padding: 0;
    font-family: ${props => applyTheme(props).fontFamilies.default};
    font-style: ${props => props.fontStyle ?? "regular"};
    font-weight: regular;
    font-size: ${props => applyTheme(props).fontSizes.small};
    line-height: ${props => applyTheme(props).lineHeights.small};
    color: ${props => applyTheme(props).colors.dark};
`;

export const PError = styled(PSmall)`
    color: ${props => applyTheme(props).colors.secondary1};
`;
