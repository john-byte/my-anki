import React, { useState, useMemo } from "react";
import { useHistory } from "react-router";
import styled from "styled-components";
import { WidthContainer } from "../common/WidthContainer";
import { VertCenterer } from "../common/VertCenterer";
import { Card } from "../common/Card";
import { Label } from "../common/Label";
import { Input } from "../common/Input";
import { Button } from "../common/Button";
import { Row } from "../common/Grid";
import { PError } from "../common/Text";
import { Dict } from '../../types';
import { useForm } from "../../hooks";
import { BACKEND_URL } from "../../constants";
import { makeFormSubmit } from "../../utils/makeFormSubmit";

import Success from "./Success";

const Stack = styled(Card)`
    & > .child + .child {
        margin-top: 8px;
    }
`;

const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const formFields = {
    "nicknameOrEmail": {
        validate: (v: string) => {
            return (v.length >= 3 && v.length <= 16) || emailRegex.test(v);
        },
        errorMessage: "Nickname/email is too short or invalid",
    },
};

const PassRecoveryScreen = () => {
    const history = useHistory();
    const [onSuccessScreen, setOnSuccessScreen] = useState(false);
    const handleSuccess = () => {
        setOnSuccessScreen(true);
    };

    const [pending, setPending] = useState(false);
    const [serviceError, setServiceError] = useState<string>();
    const handleError = (e: any) => {
        if (e.includes("not found") || e.includes("didn't confirm email")) {
            setServiceError(e);
        }
    };
    const clearServiceErrors = () => {
        setServiceError(undefined);
    };

    const formSubmit = useMemo(() => {
        return makeFormSubmit(
            `${BACKEND_URL}/auth/send-pass-recover`,
            // @ts-ignore  Type '{ email: string; username?: undefined; }' is not assignable to type 'Dict<string>'
            (fields: Dict<string>) => {
                const isEmail = emailRegex.test(fields["nicknameOrEmail"]);
                const reqBody = isEmail ? {
                    email: fields["nicknameOrEmail"],
                } : {
                    username: fields["nicknameOrEmail"],
                };

                return reqBody;
            },
            handleSuccess,
            handleError,
            (resData: any) => {
                return !!resData["success"];
            },
            "Can't send pass recoverage link. Try again later",
            () => setPending(true),
            () => setPending(false),
        );
    }, []);

    const { getValue, setValue, submit, getError } = useForm({
        fields: formFields,
        onSubmit: formSubmit,
        onFieldChange: clearServiceErrors,
    });

    return (
        <WidthContainer style={{ maxWidth: '640px' }}>
            <VertCenterer>
                {!onSuccessScreen ? (
                    <Stack>
                        <div className="child">
                            <Label>{'Nickname or email'}</Label>
                            <Input 
                                value={getValue("nicknameOrEmail")}
                                onChange={(e) => setValue("nicknameOrEmail", e.currentTarget.value)}
                            />
                            {(() => {
                                const err = getError("nicknameOrEmail");

                                return <PError>{err || serviceError}</PError>;
                            })()}
                        </div>

                        <div style={{ marginTop: '20px' }}>
                            <Row style={{ justifyContent: 'space-between' }}>
                                <Button.Primary onClick={submit} disabled={pending}>
                                    {pending ? '.....' : 'Submit'}
                                </Button.Primary>

                                <Button.Secondary onClick={() => history.push("/login")}>
                                    Login
                                </Button.Secondary>
                            </Row>
                        </div>
                    </Stack>
                ) : (
                    <Success />
                )}
            </VertCenterer>
        </WidthContainer>
    )
}

export default PassRecoveryScreen;
