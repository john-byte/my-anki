import React from "react";
import styled from "styled-components";
import { useHistory } from "react-router";
import { Card } from "../common/Card";
import { Button } from "../common/Button";
import { H2, P } from "../common/Text";

const Wrapper = styled(Card)`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const Success = () => {
    const history = useHistory();

    return (
        <Wrapper>
            <H2 style={{ textAlign: "center" }}>Success</H2>
            <P style={{ textAlign: "center", marginTop: '12px' }}>
                Password confirmation link will be sent to your email
            </P>
            <Button.Primary
                style={{ marginTop: "32px" }} 
                onClick={() => history.push('/login')}
            >
                Back to login
            </Button.Primary>
        </Wrapper>
    )
}

export default Success;
