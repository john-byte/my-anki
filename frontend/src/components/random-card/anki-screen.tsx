import React from "react";
import styled from "styled-components";
import { Card } from "../common/Card";
import Answer from "./answer";
import SelectGrade, { Grade, GradeDict } from "../select-grade";
import { updateScores } from "./api";
import { useHistory } from "react-router-dom";
import { H1 } from "../common/Text";
import { Row, Col, Stack } from "../common/Grid";
import { Button } from "../common/Button";
import { Modal } from "../common/Modal";

interface Props {
    cardName: string;
    cardTitle: string;
    deckName: string;
    front: string;
    back: string;

    answered: boolean;
    setAnswered: (v: boolean) => void;
    
    hasNextCard: boolean;
    fetchCard: () => Promise<void>;
};

const QuestionCard = styled(Card)`
    overflow: scroll;
    height: 240px;
`;

const AnkiScreen: React.FC<Props> = (props) => {
    const history = useHistory();
    const [grade, setGrade] = React.useState<Grade>(0);
    const grades = React.useRef<GradeDict>({});
    const [modalVisible, setModalVisible] = React.useState(false);
    const [answer, setAnswer] = React.useState("");

    React.useEffect(
        () => {
            if (props.cardName) {
                grades.current[props.cardName] = grade;
            }
        },
        [grade, props.cardName]
    );

    const onNext = React.useCallback(
        () => {
            props.setAnswered(false);
            props.fetchCard();
            setAnswer("");
        },
        [props.setAnswered, props.fetchCard, props.cardName, grade]
    );

    const onFinish = React.useCallback(
        () => {
            updateScores(props.deckName, grades.current)
                .then(
                    () => history.push("/finish")
                )
                .catch(
                    err => console.error(err)
                );
        },
        [props.cardName, props.deckName, grade]
    );

    return (
        <>
        <div>
            <H1 style={{ textAlign: 'center' }}>
                {props.cardTitle}
            </H1>

            <Row justify="space-between" style={{ width: "100%", marginTop: '20px' }}>
                <Col width="calc(50% - 20px / 2)">
                    <QuestionCard dangerouslySetInnerHTML={{ __html: props.front }} />
                </Col>

                {
                    props.answered ? (
                        <>
                            <div style={{ width: "20px" }} />

                            <Col width="calc(50% - 20px / 2)">
                                <QuestionCard dangerouslySetInnerHTML={{ __html: props.back }} />
                            </Col>
                        </>
                    ) : null
                    }
            </Row>

            <div style={{ height: "40px" }} />

            <Stack align='center' style={{ marginBottom: '20px' }}>
                <Button.Secondary onClick={() => setModalVisible(true)}>
                    Enter answer
                </Button.Secondary>

                <div style={{ height: "20px" }} />

                {props.answered ? (
                    <>
                        <SelectGrade 
                            onChange={setGrade}
                        />

                        <div style={{ height: "20px" }} />
                        
                        <Row>
                            {
                                props.hasNextCard ? (
                                    <>
                                        <Button.Primary
                                            onClick={onNext}
                                            disabled={!props.answered}
                                        >
                                            Next
                                        </Button.Primary>
                                        <div style={{ width: "16px" }} />
                                    </>
                                ) : null
                            }

                            {
                                props.hasNextCard ? (
                                    <Button.Secondary
                                        onClick={onFinish}
                                        disabled={!props.answered}
                                    >
                                        Finish
                                    </Button.Secondary>
                                ) : (
                                    <Button.Primary
                                        onClick={onFinish}
                                        disabled={!props.answered}
                                    >
                                        Finish
                                    </Button.Primary>
                                )
                            }
                        </Row>
                    </>
                ) : null}
            </Stack>
        </div>
        
        {modalVisible ? (
            <Modal handleClose={() => setModalVisible(false)}>
                <Answer
                    value={answer}
                    changeValue={setAnswer} 
                    onSubmit={() => {
                        props.setAnswered(true);
                        setModalVisible(false);
                    }}
                />
            </Modal>
        ) : null}
        </>
    )
};

export default AnkiScreen;