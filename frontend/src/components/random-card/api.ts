import { BACKEND_URL } from "../../constants";
import { GradeDict } from "../select-grade";
import { CardMeta } from "../../types";

export const fetchCardRefs = async (deckName: string): Promise<CardMeta[]> => {
    const sessCode = localStorage.getItem("sesscode");
    if (typeof sessCode !== "string" || !sessCode) {
        throw Error("not-auth");
    }

    const res = await fetch(
        `${BACKEND_URL}/anki/deck-cards`,
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
                "x-sesscode": sessCode,
            },
            body: JSON.stringify({
                name: deckName,
                supermemoOn: true
            })
        }
    );

    if (res.status === 403) {
        throw Error("not-auth");
    }

    const json = await res.json();
    if (!Array.isArray(json)) {
        throw Error("Invalid response");
    }

    return json;
}

export const updateScores = async (deckName: string, cards: GradeDict) => {
    const sessCode = localStorage.getItem("sesscode");
    if (typeof sessCode !== "string" || !sessCode) {
        throw Error("not-auth");
    }

    const body = await JSON.stringify({
        deck_name: deckName,
        cards
    });

    const resp = await fetch(
        `${BACKEND_URL}/anki/update-scores`,
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
                "x-sesscode": sessCode,
            },
            body
        }
    );

    if (resp.status === 403) {
        throw Error("not-auth");
    }

    return resp.json();
}
