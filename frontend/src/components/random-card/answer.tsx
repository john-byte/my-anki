import React from "react";
import styles from "./answer.module.css";
import { Button } from "../common/Button";
import { Label } from "../common/Label";
import { Row } from "../common/Grid";

interface Props {
    onSubmit: (val: string) => void;
    changeValue: (v: string) => void;
    value: string;
}

const Answer: React.FC<Props> = ({ onSubmit, changeValue, value }) => {
    const onChange = React.useCallback(
        (e: React.ChangeEvent<HTMLTextAreaElement>) => {
            changeValue(e.currentTarget.value);
        },
        []
    );

    const onRawSubmit = React.useCallback(
        (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            onSubmit(e.currentTarget.value);
        },
        [onSubmit]
    );

    return (
        <div className={styles.answer}>
            <Label>
                Your answer
            </Label>

            <textarea 
                value={value} 
                onChange={onChange}
                className={styles.input}
            >
            </textarea>

            <Row justify='flex-end' style={{ marginTop: '20px' }}>
                <Button.Primary
                    onClick={onRawSubmit}
                    disabled={value.length === 0}
                >
                    Submit
                </Button.Primary>
            </Row>
        </div>
    )
}

export default Answer;
