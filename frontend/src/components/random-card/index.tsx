import React from "react";
import { useHistory } from 'react-router';
import styled from 'styled-components';
import { useCard } from "./hooks";
import { Fence } from "../common";
import AnkiScreen from "./anki-screen";
import { H1, P } from "../common/Text";
import { Row } from '../common/Grid';
import { WidthContainer } from "../common/WidthContainer";
import { Back } from '../../assets/back';

const BackButton = styled.div`
    cursor: pointer;
    display: flex;
    align-items: center;

    > svg {
        width: 24px;
        height: auto;
    }
`;

const RandomCard: React.FC = () => {
    const card = useCard();
    const history = useHistory();

    const back = () => {
        history.push("/");
    }

    return (
        <WidthContainer style={{ maxWidth: "800px", marginTop: '64px' }}>
            <Fence visible={card.pending}>
                <H1>
                    Loading card...
                </H1>
            </Fence>

            <Fence visible={!!card.front && !!card.back}>
                <Row justify='flex-start' style={{ marginBottom: '24px' }}>
                    <BackButton onClick={back}>
                        <Back /> 
                        <P style={{ marginLeft: '14px' }}>To decks</P>
                    </BackButton>
                </Row>

                <AnkiScreen
                    cardName={card.cardName ?? ""}
                    cardTitle={card.cardTitle ?? ""}
                    deckName={card.deckName ?? ""} 
                    front={card.front ?? ""}
                    back={card.back ?? ""}
                    answered={card.answered}
                    setAnswered={card.setAnswered}
                    hasNextCard={card.hasNextCard}
                    fetchCard={card.fetchCard}
                />
            </Fence>

            <Fence visible={!!card.error}>
                <H1>
                    Error occured: 
                    <br />
                    {card.error?.message ?? ""}
                </H1>
            </Fence>
        </WidthContainer>
    )
}

export default RandomCard;
