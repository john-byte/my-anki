import React, { useLayoutEffect, useState, useRef, useMemo } from "react";
import styled from "styled-components";
import { useHistory } from "react-router";
import { WidthContainer } from "../common/WidthContainer";
import { Card } from "../common/Card";
import { Row, Col } from "../common/Grid";
import { Button } from "../common/Button";
import { H2, H3, P } from "../common/Text";
import { makeGetAnkiCardsMeta } from "./api-cards";
import { useAsyncMemo } from "../../hooks";
import { Back } from '../../assets/back';
import CreateAnkiCard from "./CreateAnkiCard";
import EditAnkiCard from "./EditAnkiCard";
import DeleteAnkiCard from "./DeleteAnkiCard";

// TODO: Move helper style components into separate file
const AnkiCardsWrapper = styled(Row)`
    margin-top: 28px;

    .child {
        width: 25%;
        min-height: 160px;
        margin-top: 12px;

        &:nth-child(1), &:nth-child(2), &:nth-child(3) {
            margin-top: 0;
        }

        &:not(:nth-child(3n+1)) {
            margin-left: 12px;
        }
    }
`;

const AnkiCardCardWrapper = styled(Card)`
    cursor: pointer;

    transition: 0.2s;
    transform: translateY(0);

    &:hover {
        transform: translateY(-8px);
    }
`;

const CardActionButton = styled(Button.Secondary)`
    min-width: 32px;
    max-width: 64px;
    min-height: 32px;
`;

const BackButton = styled.div`
    cursor: pointer;
    width: 48px;
    height: auto;

    > svg {
        width: 100%;
        height: 100%;
    }
`;

interface AnkiCardCardProps {
    uid: string;
    title: string;
    onSelect?: (uid: string, title: string) => void;
    onEdit: (uid: string, title: string) => void; 
    onDelete: (uid: string, title: string) => void;
}
const AnkiCardCard = ({ uid, title, onSelect, onEdit, onDelete }: AnkiCardCardProps) => {
    const edit = () => {
        onEdit(uid, title);
    };
    const remove = () => {
        onDelete(uid, title);
    };
    const select = (e: React.MouseEvent<HTMLElement>) => {
        e.stopPropagation();
        
        if (onSelect && e.target === e.currentTarget) {
            onSelect(uid, title);
        }
    };

    // TODO: Add icons for "edit" & "delete" actions
    return (
        <AnkiCardCardWrapper onClick={select}>
            <Row justify="flex-end">
                <CardActionButton
                    style={{ marginRight: "12px" }}
                    onClick={edit}
                >
                    Edit
                </CardActionButton>
                <CardActionButton onClick={remove}>
                    X
                </CardActionButton>
            </Row>

            <P style={{ marginTop: "24px", textAlign: "center" }}>
                {title}
            </P>
        </AnkiCardCardWrapper>
    )
};

interface AnkiCardsPanelProps {
    selectedDeck: { uid: string; title: string };
    returnToDecks: () => void;
}
const AnkiCardsPanel = ({ selectedDeck, returnToDecks }: AnkiCardsPanelProps) => {
    const history = useHistory();
    const getAnkiCardsMeta = useMemo(() => {
        return makeGetAnkiCardsMeta(selectedDeck.uid);
    }, [selectedDeck]);
    const { pending, error, data, refetch } = useAsyncMemo(getAnkiCardsMeta);

    const [createAnkiCardVisible, setCreateAnkiCardVisible] = useState(false);
    const [editAnkiCardVisible, setEditAnkiCardVisible] = useState(false);
    const [deleteAnkiCardVisible, setDeleteAnkiCardVisible] = useState(false);
    
    const [selectedAnkiCard, setSelectedAnkiCard] = useState<{ uid: string; title: string; }>();
    const selectAnkiCard = (uid: string, title: string) => {
        setSelectedAnkiCard({ uid, title });
    };

    const handleEdit = (uid: string, title: string) => {
        selectAnkiCard(uid, title);
        setEditAnkiCardVisible(true);
    };
    const handleDelete = (uid: string, title: string) => {
        selectAnkiCard(uid, title);
        setDeleteAnkiCardVisible(true);
    };

    useLayoutEffect(() => {
        if (error?.message === "not-auth") {
            history.push("/login");
        }
    }, [error]);

    return (
        <>
            <WidthContainer style={{ maxWidth: "800px", marginTop: "64px" }}>
                {pending ? (
                    <H2 style={{ textAlign: "center" }}>Loading cards...</H2>
                ) : error ? (
                    <H2 style={{ textAlign: "center" }}>{error.message}</H2>
                ) : data ? (
                    <>
                        <Row justify="space-between" align='center'>
                            <BackButton onClick={returnToDecks}>
                                <Back />
                            </BackButton>

                            <H3>{selectedDeck.title || selectedDeck.uid}</H3>

                            <Button.Primary onClick={() => setCreateAnkiCardVisible(true)}>
                                Add card
                            </Button.Primary>
                        </Row>

                        {data.length ? (
                            <AnkiCardsWrapper>
                                {data.map((e) => {
                                    return (
                                        <Col
                                            key={e.uid} 
                                            className="child"
                                        >
                                            <AnkiCardCard 
                                                uid={e.uid}
                                                title={e.title}
                                                onEdit={handleEdit}
                                                onDelete={handleDelete}
                                            />
                                        </Col>
                                    )
                                })}
                            </AnkiCardsWrapper>
                        ) : (
                            <H2 style={{ textAlign: "center", marginTop: "12px" }}>
                                You have no cards. Let's create first one!
                            </H2>
                        )}
                    </>
                ) : (
                    <H2 style={{ textAlign: "center" }}>Could not load data :(</H2>
                )}
            </WidthContainer>

            {createAnkiCardVisible ? (
                <CreateAnkiCard
                    deckUid={selectedDeck.uid} 
                    handleClose={() => setCreateAnkiCardVisible(false)}
                    refetch={refetch}
                />
            ) : null}
            {editAnkiCardVisible ? (
                <EditAnkiCard
                    deckUid={selectedDeck.uid} 
                    uid={selectedAnkiCard!.uid}
                    oldTitle={selectedAnkiCard!.title}
                    handleClose={() => setEditAnkiCardVisible(false)}
                    refetch={refetch}
                />
            ) : null}
            {deleteAnkiCardVisible ? (
                <DeleteAnkiCard 
                    uid={selectedAnkiCard!.uid}
                    title={selectedAnkiCard!.title}
                    handleClose={() => setDeleteAnkiCardVisible(false)}
                    refetch={refetch}
                />
            ) : null}
        </>
    )
}

export default AnkiCardsPanel;
