import React, { useState } from "react";
import { Modal } from "../common/Modal";
import { Label } from "../common/Label";
import { Input } from "../common/Input";
import { Button } from "../common/Button";
import { H3 } from "../common/Text";
import { editDeck } from "./api";

interface Props {
    uid: string;
    title: string;
    handleClose: () => void;
    refetch: () => void;
}

const EditDeck = ({ uid, title: oldTitle, handleClose, refetch }: Props) => {
    const [title, setTitle] = useState(oldTitle);
    const [pending, setPending] = useState(false);
    
    const handleSubmit = async () => {
        try {
            setPending(true);

            const result = await editDeck(uid, title);
            if (!result.success) {
                throw "Can't edit deck";
            }

            refetch();
            handleClose();
        } catch (e) {
            console.error(e);
        } finally {
            setPending(false);
        }
    };

    return (
        <Modal handleClose={handleClose}>
            <H3 style={{ marginBottom: "16px" }}>Editing deck "{oldTitle}"</H3>
                
            <Label>New title</Label>
            <Input value={title} onChange={(e) => setTitle(e.currentTarget.value)} />

            <Button.Primary 
                style={{ marginTop: "20px" }}
                disabled={!title || title === oldTitle || pending}
                onClick={handleSubmit}
            >
                {pending ? '...' : 'Submit'}
            </Button.Primary>
        </Modal>
    )
};

export default EditDeck;
