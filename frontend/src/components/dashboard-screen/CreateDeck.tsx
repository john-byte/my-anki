import React, { useState } from "react";
import { Modal } from "../common/Modal";
import { Label } from "../common/Label";
import { Input } from "../common/Input";
import { Button } from "../common/Button";
import { createDeck } from "./api";

interface Props {
    handleClose: () => void;
    refetch: () => void;
}

const CreateDeck = ({ handleClose, refetch }: Props) => {
    const [title, setTitle] = useState("");
    const [pending, setPending] = useState(false);
    
    const handleSubmit = async () => {
        try {
            setPending(true);

            const result = await createDeck(title);
            if (!result.success) {
                throw "Can't create deck";
            }

            refetch();
            handleClose();
        } catch (e) {
            console.error(e);
        } finally {
            setPending(false);
        }
    };

    return (
        <Modal handleClose={handleClose}>
            <Label>Title</Label>
            <Input value={title} onChange={(e) => setTitle(e.currentTarget.value)} />

            <Button.Primary 
                style={{ marginTop: "20px" }}
                disabled={!title || pending}
                onClick={handleSubmit}
            >
                {pending ? '...' : 'Submit'}
            </Button.Primary>
        </Modal>
    )
};

export default CreateDeck;
