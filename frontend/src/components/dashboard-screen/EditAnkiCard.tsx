import React, { useState, useEffect } from "react";
import { Modal } from "../common/Modal";
import { Label } from "../common/Label";
import { Input } from "../common/Input";
import { Button } from "../common/Button";
import { H2, H3 } from "../common/Text";
import { Row, Col } from "../common/Grid";
import { Textarea } from "../common/Textarea";
import { editAnkiCard } from "./api-cards";
import { fetchCardSides } from '../../api';

interface Props {
    deckUid: string;
    uid: string;
    oldTitle: string;
    handleClose: () => void;
    refetch: () => void;
}

const EditAnkiCard = ({ deckUid, uid, oldTitle, handleClose, refetch }: Props) => {
    const [sidesPending, setSidesPending] = useState(true);
    const [sidesLoadError, setSidesLoadError] = useState<Error>();
    const [oldFrontSide, setOldFrontSide] = useState<string>();
    const [oldBackSide, setOldBackSide] = useState<string>();
    useEffect(() => {
        setSidesPending(true);
        setOldFrontSide(undefined);
        setOldBackSide(undefined);
        setSidesLoadError(undefined);

        fetchCardSides(deckUid, uid)
            .then((res) => {
                setOldFrontSide(res.front);
                setOldBackSide(res.back);
            })
            .catch((err) => {
                console.error(err);
                setSidesLoadError(err);
            });
    }, [deckUid, uid]);

    const [title, setTitle] = useState(oldTitle);
    const [frontSide, setFrontSide] = useState('');
    const [backSide, setBackSide] = useState('');
    const [pending, setPending] = useState(false);

    useEffect(() => {
        if (oldFrontSide) {
            setFrontSide(oldFrontSide);
        }

        if (oldBackSide) {
            setBackSide(oldBackSide);
        }

        if (oldFrontSide && oldBackSide) {
            setSidesPending(false);
        }
    }, [oldFrontSide, oldBackSide]);
    
    const handleSubmit = async () => {
        try {
            setPending(true);

            const result = await editAnkiCard(
                uid,
                deckUid, 
                title !== oldTitle ? title : undefined,
                frontSide !== oldFrontSide ? frontSide : undefined,
                backSide !== oldBackSide ? backSide : undefined,
            );
            if (!result.success) {
                throw "Can't edit card";
            }

            refetch();
            handleClose();
        } catch (e) {
            console.error(e);
        } finally {
            setPending(false);
        }
    };

    const submitDisabled = !(title && frontSide && backSide) ||
        (title === oldTitle && frontSide === oldFrontSide && backSide === oldBackSide) ||
        pending;

    return (
        <Modal handleClose={handleClose}>
            {sidesPending ? (
                <H2>Loading card sides...</H2>
            ) : sidesLoadError ? (
                <H2>Unable to load card sides :(</H2>
            ) : (
                <>
                    <H3 style={{ marginBottom: "16px" }}>Editing card "{oldTitle}"</H3>
                    
                    <Label>New title</Label>
                    <Input value={title} onChange={(e) => setTitle(e.currentTarget.value)} />
        
                    <Row justify='space-between' style={{ marginTop: '12px', width: '100%' }}>
                        <Col width="calc(50% - 6px)">
                            <Label>New front side</Label>
                            <Textarea 
                                value={frontSide}
                                style={{ height: '250px' }}
                                onChange={(e) => setFrontSide(e.currentTarget.value)}
                            />
                        </Col>
        
                        <Col width="calc(50% - 6px)">
                            <Label>New back side</Label>
                            <Textarea 
                                value={backSide}
                                style={{ height: '250px' }}
                                onChange={(e) => setBackSide(e.currentTarget.value)}
                            />
                        </Col>
                    </Row>
        
                    <Button.Primary 
                        style={{ marginTop: "20px" }}
                        disabled={submitDisabled}
                        onClick={handleSubmit}
                    >
                        {pending ? '...' : 'Submit'}
                    </Button.Primary>
                </>
            )}
        </Modal>
    )
};

export default EditAnkiCard;
