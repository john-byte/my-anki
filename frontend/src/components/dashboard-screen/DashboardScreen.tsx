import React, { useLayoutEffect, useState } from "react";
import DecksPanel from "./DecksPanel";
import CardsPanel from "./CardsPanel";

const DashboardScreen = () => {
    const [selectedDeck, setSelectedDeck] = useState<{ uid: string; title: string }>();
    const goToCards = (uid: string, title: string) => {
        setSelectedDeck({ uid, title });
    };
    const goBackToDecks = () => setSelectedDeck(undefined);

    return selectedDeck ? (
        <CardsPanel
            selectedDeck={selectedDeck}
            returnToDecks={goBackToDecks}
        />
    ) : (
        <DecksPanel onDeckSelect={goToCards} />
    );
}

export default DashboardScreen;
