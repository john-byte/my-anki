import React, { useState } from "react";
import { Modal } from "../common/Modal";
import { Label } from "../common/Label";
import { Input } from "../common/Input";
import { Button } from "../common/Button";
import { Row, Col } from "../common/Grid";
import { Textarea } from "../common/Textarea";
import { createAnkiCard } from "./api-cards";

interface Props {
    deckUid: string;
    handleClose: () => void;
    refetch: () => void;
}

const CreateAnkiCard = ({ deckUid, handleClose, refetch }: Props) => {
    const [title, setTitle] = useState("");
    const [frontSide, setFrontSide] = useState("");
    const [backSide, setBackSide] = useState("");
    const [pending, setPending] = useState(false);
    
    const handleSubmit = async () => {
        try {
            setPending(true);

            const result = await createAnkiCard(
                deckUid,
                title,
                frontSide,
                backSide,
            );
            if (!result.success) {
                throw "Can't create card";
            }

            refetch();
            handleClose();
        } catch (e) {
            console.error(e);
        } finally {
            setPending(false);
        }
    };

    return (
        <Modal handleClose={handleClose}>
            <Label>Title</Label>
            <Input value={title} onChange={(e) => setTitle(e.currentTarget.value)} />

            <Row justify='space-between' style={{ marginTop: '12px', width: '100%' }}>
                <Col width="calc(50% - 6px)">
                    <Label>Front side</Label>
                    <Textarea 
                        value={frontSide}
                        style={{ height: '250px' }}
                        onChange={(e) => setFrontSide(e.currentTarget.value)}
                    />
                </Col>

                <Col width="calc(50% - 6px)">
                    <Label>Back side</Label>
                    <Textarea 
                        value={backSide}
                        style={{ height: '250px' }}
                        onChange={(e) => setBackSide(e.currentTarget.value)}
                    />
                </Col>
            </Row>

            <Button.Primary 
                style={{ marginTop: "20px" }}
                disabled={!title || !frontSide || !backSide || pending}
                onClick={handleSubmit}
            >
                {pending ? '...' : 'Submit'}
            </Button.Primary>
        </Modal>
    )
};

export default CreateAnkiCard;
