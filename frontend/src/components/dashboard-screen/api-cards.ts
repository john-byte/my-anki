import { BACKEND_URL } from "../../constants";
import { AnkiCardMeta } from "../../types";

export const makeGetAnkiCardsMeta = (deckUid: string) => 
async (abortSig?: AbortSignal): Promise<AnkiCardMeta[]> => {
    const sessCode = localStorage.getItem("sesscode");

    if (!sessCode) {
        throw Error("not-auth");
    }

    const resp = await fetch(
        `${BACKEND_URL}/cms/cards/all?deck=${deckUid}`,
        {
            method: "GET",
            headers: {
                "x-sesscode": sessCode,
            },
            signal: abortSig,
        },
    );

    if (resp.status === 403) {
        throw Error("not-auth");
    }

    const json = await resp.json();
    return json;
}

export const createAnkiCard = async (
    deckUid: string,
    title: string,
    frontSide: string,
    backSide: string,
): Promise<{ success: boolean; }> => {
    const sessCode = localStorage.getItem("sesscode");

    if (!sessCode) {
        throw Error("not-auth");
    }

    const resp = await fetch(
        `${BACKEND_URL}/cms/cards/add`,
        {
            method: "POST",
            headers: {
                "x-sesscode": sessCode,
                "content-type": "application/json",
            },
            body: JSON.stringify({
                deckUid,
                title,
                frontSide,
                backSide,
            })
        },
    );

    if (resp.status === 403) {
        throw Error("not-auth");
    }

    const json = await resp.json();
    return json;
}

/**
 * Edits deck with given uid
 * ('undefined' value for a field means that it remains unchanged)
 * @param uid 
 * @param deckUid 
 * @param title 
 * @param frontSide
 * @param backSide
 * @returns 
 */
export const editAnkiCard = async (
    uid: string,
    deckUid: string, 
    title: string | undefined,
    frontSide: string | undefined,
    backSide: string | undefined,
): Promise<{ success: boolean; }> => {
    const sessCode = localStorage.getItem("sesscode");

    if (!sessCode) {
        throw Error("not-auth");
    }

    const updateQuery: any = {
        uid,
        deckUid,
    };
    if (typeof title === 'string') {
        updateQuery.title = title;
    } 
    if (typeof frontSide === 'string') {
        updateQuery.frontSide = frontSide;
    }
    if (typeof backSide === 'string') {
        updateQuery.backSide = backSide;
    }

    const resp = await fetch(
        `${BACKEND_URL}/cms/cards/edit`,
        {
            method: "POST",
            headers: {
                "x-sesscode": sessCode,
                "content-type": "application/json",
            },
            body: JSON.stringify(updateQuery)
        },
    );

    if (resp.status === 403) {
        throw Error("not-auth");
    }

    const json = await resp.json();
    return json;
}

export const removeAnkiCard = async (uid: string): Promise<{ success: boolean; }> => {
    const sessCode = localStorage.getItem("sesscode");

    if (!sessCode) {
        throw Error("not-auth");
    }

    const resp = await fetch(
        `${BACKEND_URL}/cms/cards/remove`,
        {
            method: "POST",
            headers: {
                "x-sesscode": sessCode,
                "content-type": "application/json",
            },
            body: JSON.stringify({
                uid,
            })
        },
    );

    if (resp.status === 403) {
        throw Error("not-auth");
    }

    const json = await resp.json();
    return json;
};
