import React, { useState } from "react";
import { Modal } from "../common/Modal";
import { Button } from "../common/Button";
import { Row } from "../common/Grid";
import { H3, P } from "../common/Text";
import { removeDeck } from "./api";
import { DefaultTheme } from "../common/themes";

interface Props {
    uid: string;
    title: string;
    handleClose: () => void;
    refetch: () => void;
}

const DeleteDeck = ({ uid, title, handleClose, refetch }: Props) => {
    const [pending, setPending] = useState(false);
    
    const handleSubmit = async () => {
        try {
            setPending(true);

            const result = await removeDeck(uid);
            if (!result.success) {
                throw "Can't remove deck";
            }

            refetch();
            handleClose();
        } catch (e) {
            console.error(e);
        } finally {
            setPending(false);
        }
    };

    return (
        <Modal handleClose={handleClose}>
            <H3 style={{ textAlign: "center" }}>
                Are you sure to delete deck "{title}"?
            </H3>
            <P style={{ marginTop: "8px", textAlign: "center", color: DefaultTheme.colors.secondary1 }}>
                This action is NOT REVERSIBLE!
            </P>

            <Row justify="center" style={{ marginTop: "56px" }}>
                <Button.Secondary
                    disabled={pending}
                    onClick={handleClose}
                >
                    {pending ? '...' : 'Cancel'}
                </Button.Secondary>

                <Button.Danger
                    style={{ marginLeft: "16px" }}
                    disabled={pending}
                    onClick={handleSubmit}
                >
                    {pending ? '...' : 'Delete'}
                </Button.Danger>
            </Row>
        </Modal>
    )
};

export default DeleteDeck;
