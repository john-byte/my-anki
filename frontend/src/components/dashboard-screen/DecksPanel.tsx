import React, { useLayoutEffect, useState, useRef } from "react";
import styled from "styled-components";
import { useHistory } from "react-router";
import { WidthContainer } from "../common/WidthContainer";
import { Card } from "../common/Card";
import { Row, Col } from "../common/Grid";
import { Button } from "../common/Button";
import { H2, P } from "../common/Text";
import { getDecks } from "./api";
import { useAsyncMemo } from "../../hooks";
import CreateDeck from "./CreateDeck";
import EditDeck from "./EditDeck";
import DeleteDeck from "./DeleteDeck";

// TODO: Move helper style components into separate file
const DecksWrapper = styled(Row)`
    margin-top: 12px;

    .child {
        width: 25%;
        min-height: 160px;
        margin-top: 12px;

        &:nth-child(1), &:nth-child(2), &:nth-child(3) {
            margin-top: 0;
        }

        &:not(:nth-child(3n+1)) {
            margin-left: 12px;
        }
    }
`;

const DeckCardWrapper = styled(Card)`
    cursor: pointer;

    transition: 0.2s;
    transform: translateY(0);

    &:hover {
        transform: translateY(-8px);
    }
`;

const CardActionButton = styled(Button.Secondary)`
    min-width: 32px;
    max-width: 64px;
    min-height: 32px;
`;

interface DeckCardProps {
    uid: string;
    title: string;
    onSelect: (uid: string, title: string) => void;
    onEdit: (uid: string, title: string) => void; 
    onDelete: (uid: string, title: string) => void;
}
const DeckCard = ({ uid, title, onSelect, onEdit, onDelete }: DeckCardProps) => {
    const editBtn = useRef<HTMLButtonElement>(null);
    const deleteBtn = useRef<HTMLButtonElement>(null);

    const edit = () => {
        onEdit(uid, title);
    };
    const remove = () => {
        onDelete(uid, title);
    };
    const select = (e: React.MouseEvent<HTMLElement>) => {        
        if (e.target !== editBtn.current &&
            // @ts-ignore Argument of type 'EventTarget' is not assignable to parameter of type 'Node'.
            !editBtn?.current?.contains(e.target) &&
            e.target !== deleteBtn.current &&
            // @ts-ignore Argument of type 'EventTarget' is not assignable to parameter of type 'Node'.
            !deleteBtn?.current?.contains(e.target)) 
        {
            onSelect(uid, title);
        }
    };

    // TODO: Add icons for "edit" & "delete" actions
    return (
        <DeckCardWrapper onClick={select}>
            <Row justify="flex-end">
                <CardActionButton
                    ref={editBtn}
                    style={{ marginRight: "12px" }}
                    onClick={edit}
                >
                    Edit
                </CardActionButton>
                <CardActionButton ref={deleteBtn} onClick={remove}>
                    X
                </CardActionButton>
            </Row>

            <P style={{ marginTop: "24px", textAlign: "center" }}>
                {title}
            </P>
        </DeckCardWrapper>
    )
};

interface DecksPanelProps {
    onDeckSelect: (uid: string, title: string) => void;
}
const DecksPanel = ({ onDeckSelect }: DecksPanelProps) => {
    const history = useHistory();
    const { pending, error, data, refetch } = useAsyncMemo(getDecks);

    const [createDeckVisible, setCreateDeckVisible] = useState(false);
    const [editDeckVisible, setEditDeckVisible] = useState(false);
    const [deleteDeckVisible, setDeleteDeckVisible] = useState(false);
    
    const [selectedDeck, setSelectedDeck] = useState<{ uid: string; title: string; }>();
    const selectDeck = (uid: string, title: string) => {
        setSelectedDeck({ uid, title });
    };

    const handleEdit = (uid: string, title: string) => {
        selectDeck(uid, title);
        setEditDeckVisible(true);
    };
    const handleDelete = (uid: string, title: string) => {
        selectDeck(uid, title);
        setDeleteDeckVisible(true);
    };

    useLayoutEffect(() => {
        if (error?.message === "not-auth") {
            history.push("/login");
        }
    }, [error]);

    return (
        <>
            <WidthContainer style={{ maxWidth: "800px", marginTop: "64px" }}>
                {pending ? (
                    <H2 style={{ textAlign: "center" }}>Loading decks...</H2>
                ) : error ? (
                    <H2 style={{ textAlign: "center" }}>{error.message}</H2>
                ) : data ? (
                    <>
                        <Row justify="flex-end">
                            <Button.Primary onClick={() => setCreateDeckVisible(true)}>
                                Add deck
                            </Button.Primary>
                        </Row>

                        {data.length ? (
                            <DecksWrapper>
                                {data.map((e) => {
                                    return (
                                        <Col
                                            key={e.uid} 
                                            className="child"
                                        >
                                            <DeckCard 
                                                uid={e.uid}
                                                title={e.title}
                                                onSelect={onDeckSelect}
                                                onEdit={handleEdit}
                                                onDelete={handleDelete}
                                            />
                                        </Col>
                                    )
                                })}
                            </DecksWrapper>
                        ) : (
                            <H2 style={{ textAlign: "center", marginTop: "12px" }}>
                                You have no decks. Let's create first one!
                            </H2>
                        )}
                    </>
                ) : (
                    <H2 style={{ textAlign: "center" }}>Could not load data :(</H2>
                )}
            </WidthContainer>

            {createDeckVisible ? (
                <CreateDeck 
                    handleClose={() => setCreateDeckVisible(false)}
                    refetch={refetch}
                />
            ) : null}
            {editDeckVisible ? (
                <EditDeck 
                    uid={selectedDeck!.uid}
                    title={selectedDeck!.title}
                    handleClose={() => setEditDeckVisible(false)}
                    refetch={refetch}
                />
            ) : null}
            {deleteDeckVisible ? (
                <DeleteDeck 
                    uid={selectedDeck!.uid}
                    title={selectedDeck!.title}
                    handleClose={() => setDeleteDeckVisible(false)}
                    refetch={refetch}
                />
            ) : null}
        </>
    )
}

export default DecksPanel;
