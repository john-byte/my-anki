import { BACKEND_URL } from "../../constants";
import { Deck } from "../../types";

export const getDecks = async (abortSig?: AbortSignal): Promise<Deck[]> => {
    const sessCode = localStorage.getItem("sesscode");

    if (!sessCode) {
        throw Error("not-auth");
    }

    const resp = await fetch(
        `${BACKEND_URL}/cms/decks/all`,
        {
            method: "GET",
            headers: {
                "x-sesscode": sessCode,
            },
            signal: abortSig,
        },
    );

    if (resp.status === 403) {
        throw Error("not-auth");
    }

    const json = await resp.json();
    return json;
}

export const createDeck = async (title: string): Promise<{ success: boolean; }> => {
    const sessCode = localStorage.getItem("sesscode");

    if (!sessCode) {
        throw Error("not-auth");
    }

    const resp = await fetch(
        `${BACKEND_URL}/cms/decks/add`,
        {
            method: "POST",
            headers: {
                "x-sesscode": sessCode,
                "content-type": "application/json",
            },
            body: JSON.stringify({
                title,
            })
        },
    );

    if (resp.status === 403) {
        throw Error("not-auth");
    }

    const json = await resp.json();
    return json;
}

export const editDeck = async (uid: string, title: string): Promise<{ success: boolean; }> => {
    const sessCode = localStorage.getItem("sesscode");

    if (!sessCode) {
        throw Error("not-auth");
    }

    const resp = await fetch(
        `${BACKEND_URL}/cms/decks/edit`,
        {
            method: "POST",
            headers: {
                "x-sesscode": sessCode,
                "content-type": "application/json",
            },
            body: JSON.stringify({
                uid,
                title,
            })
        },
    );

    if (resp.status === 403) {
        throw Error("not-auth");
    }

    const json = await resp.json();
    return json;
}

export const removeDeck = async (uid: string): Promise<{ success: boolean; }> => {
    const sessCode = localStorage.getItem("sesscode");

    if (!sessCode) {
        throw Error("not-auth");
    }

    const resp = await fetch(
        `${BACKEND_URL}/cms/decks/remove`,
        {
            method: "POST",
            headers: {
                "x-sesscode": sessCode,
                "content-type": "application/json",
            },
            body: JSON.stringify({
                uid,
            })
        },
    );

    if (resp.status === 403) {
        throw Error("not-auth");
    }

    const json = await resp.json();
    return json;
};
