import React, { useState, useMemo } from "react";
import { useHistory } from "react-router";
import styled from "styled-components";
import { WidthContainer } from "../common/WidthContainer";
import { VertCenterer } from "../common/VertCenterer";
import { Card } from "../common/Card";
import { Label } from "../common/Label";
import { Input } from "../common/Input";
import { Button } from "../common/Button";
import { Row } from "../common/Grid";
import { PError } from "../common/Text";
import { Dict } from '../../types';
import { useForm } from "../../hooks";
import { BACKEND_URL } from "../../constants";
import { makeFormSubmit } from "../../utils/makeFormSubmit";

const Stack = styled(Card)`
    & > .child + .child {
        margin-top: 8px;
    }
`;

const formFields = {
    "password": {
        validate: (v: string) => {
            return v.length >= 8;
        },
        errorMessage: "Password is too short",
    },
    "repPassword": {
        validate: (v: string, fields?: Dict<string>) => {
            return fields?.["password"] === v;
        },
        errorMessage: "Passwords must match",
    },
};

const NewPasswordScreen = () => {
    const history = useHistory();
    const handleSuccess = () => {
        history.push("/login");
    };

    const [pending, setPending] = useState(false);
    const [serviceError, setServiceError] = useState<string>();
    const handleError = (e: any) => {
        if (e.includes("not found") || e.includes("update password")) {
            setServiceError(e);
        }
    };
    const clearServiceErrors = () => {
        setServiceError(undefined);
    };

    const queryParams = new URLSearchParams(window.location.search);
    const code = queryParams.get("code");
    const formSubmit = useMemo(() => {
        return makeFormSubmit(
            `${BACKEND_URL}/auth/recover-password`,
            (fields: Dict<string>) => {
                const req: any = {
                    newPassword: fields["password"],
                    code: code ?? '',
                };
                delete req["repPassword"];
                return req;
            },
            handleSuccess,
            handleError,
            (resData: any) => {
                return !!resData["success"];
            },
            "Can't recover password. Try again later",
            () => setPending(true),
            () => setPending(false),
        );
    }, []);

    const { getValue, setValue, submit, getError } = useForm({
        fields: formFields,
        onSubmit: formSubmit,
        onFieldChange: clearServiceErrors,
    });

    if (!code) {
        history.push("/login");
        return null;
    }
 
    return (
        <WidthContainer style={{ maxWidth: '640px' }}>
            <VertCenterer>
                    <Stack>
                        <div className="child">
                            <Label>{'Password'}</Label>
                            <Input 
                                value={getValue("password")}
                                onChange={(e) => setValue("password", e.currentTarget.value)}
                                type="password"
                            />
                            {(() => {
                                const err = getError("password");

                                return <PError>{err || serviceError}</PError>;
                            })()}
                        </div>

                        <div className="child">
                            <Label>{'Repeat password'}</Label>
                            <Input 
                                value={getValue("repPassword")}
                                onChange={(e) => setValue("repPassword", e.currentTarget.value)}
                                type="password"
                            />
                            {(() => {
                                const err = getError("repPassword");

                                return <PError>{err}</PError>;
                            })()}
                        </div>

                        <div style={{ marginTop: '20px' }}>
                            <Row style={{ justifyContent: 'space-between' }}>
                                <Button.Primary onClick={submit} disabled={pending}>
                                    {pending ? '.....' : 'Submit'}
                                </Button.Primary>
                            </Row>
                        </div>
                    </Stack>
            </VertCenterer>
        </WidthContainer>
    )
}

export default NewPasswordScreen;
