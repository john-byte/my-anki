import React, { useState, useMemo } from "react";
import { useHistory } from "react-router";
import styled from "styled-components";
import { WidthContainer } from "../common/WidthContainer";
import { VertCenterer } from "../common/VertCenterer";
import { Card } from "../common/Card";
import { Label } from "../common/Label";
import { Input } from "../common/Input";
import { Button } from "../common/Button";
import { Row } from "../common/Grid";
import { PError } from "../common/Text";
import { useForm } from "../../hooks";
import { BACKEND_URL } from "../../constants";
import { makeFormSubmit } from "../../utils/makeFormSubmit";
import { Dict } from "../../types";

const Stack = styled(Card)`
    & > .child + .child {
        margin-top: 8px;
    }
`;

const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const formFields = {
    "nicknameOrEmail": {
        validate: (v: string) => {
            return (v.length >= 3 && v.length <= 16) || emailRegex.test(v);
        },
        errorMessage: "Nickname/email is invalid",
    },
    "password": {
        validate: (v: string) => {
            return v.length >= 8;
        },
        errorMessage: "Password is too short",
    },
};

const LoginScreen = () => {
    const history = useHistory();
    const handleSuccess = (resData: any) => {
        localStorage.setItem("sesscode", resData["sessCode"]);
        history.push("/");
    };

    const [pending, setPending] = useState(false);
    const [loginServiceError, setLoginServiceError] = useState<string>();
    const [passwordServiceError, setPasswordServiceError] = useState<string>();
    const handleError = (e: any) => {
        if (e?.includes?.("doesn't exist")) {
            setLoginServiceError(e);
        } else if (e?.includes?.("Invalid password")) {
            setPasswordServiceError(e);
        }
    };
    const clearServiceErrors = () => {
        setLoginServiceError(undefined);
        setPasswordServiceError(undefined);
    };

    const formSubmit = useMemo(() => {
        return makeFormSubmit(
            `${BACKEND_URL}/auth/login`,
            // @ts-ignore  Type '{ email: string; password: string; username?: undefined; }' is not assignable to type 'Dict<string>'
            (fields: Dict<string>) => {
                const isEmail = emailRegex.test(fields["nicknameOrEmail"]);
                const reqBody = isEmail ? {
                    email: fields["nicknameOrEmail"],
                    password: fields["password"],
                } : {
                    username: fields["nicknameOrEmail"],
                    password: fields["password"],
                };

                return reqBody;
            },
            handleSuccess,
            handleError,
            (resData: any) => {
                if (!resData || !resData["success"]) {
                    return false;
                } 

                const sessCode = resData["sessCode"];
                return typeof sessCode === 'string' && !!sessCode.length;
            },
            "Login failed",
            () => setPending(true),
            () => setPending(false),
        );
    }, []);

    const { getValue, setValue, submit, getError } = useForm({
        fields: formFields,
        onSubmit: formSubmit,
        onFieldChange: clearServiceErrors,
    });

    return (
        <WidthContainer style={{ maxWidth: '640px' }}>
            <VertCenterer>
                <Stack>
                    <div className="child">
                        <Label>{'Nickname or email'}</Label>
                        <Input 
                            value={getValue("nicknameOrEmail")}
                            onChange={(e) => setValue("nicknameOrEmail", e.currentTarget.value)}
                        />
                        {(() => {
                            const err = getError("nicknameOrEmail");

                            return <PError>{err || loginServiceError}</PError>
                        })()}
                    </div>

                    <div className="child">
                        <Label>{'Password'}</Label>
                        <Input 
                            value={getValue("password")}
                            onChange={(e) => setValue("password", e.currentTarget.value)}
                            type="password"
                        />
                        {(() => {
                            const err = getError("password");

                            return <PError>{err || passwordServiceError}</PError>
                        })()}
                        <Button.Link 
                            style={{ 
                                marginTop: '4px',
                                // TODO: apply theme
                                fontSize: '12px',
                                lineHeight: '20px' 
                            }}
                            onClick={() => history.push("/pass-recovery")}
                        >
                            Forgot your password?
                        </Button.Link>
                    </div>

                    <div style={{ marginTop: '20px' }}>
                        <Row style={{ justifyContent: "space-between" }}>
                            <Button.Primary onClick={submit} disabled={pending}>
                                {pending ? '.....' : 'Submit'}
                            </Button.Primary>

                            <Button.Secondary onClick={() => history.push("/register")}>
                                Register
                            </Button.Secondary>
                        </Row>
                    </div>
                </Stack>
            </VertCenterer>
        </WidthContainer>
    )
}

export default LoginScreen;
