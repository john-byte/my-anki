import React from "react";
import styles from "./index.module.css";
import { P } from "../common/Text";

export type Grade = 0 | 1 | 2 | 3 | 4 | 5;
const grades: Grade[] = [0, 1, 2, 3, 4, 5];
export interface GradeDict {
    [cardName: string]: Grade;
}

interface Props {
    onChange: (v: Grade) => void;
}

const SelectGrade: React.FC<Props> = ({ onChange }) => {
    return (
        <div>
            <P style={{ textAlign: 'center' }}>
                Your estimate
            </P>

            <div>
                {grades.map(grade => (
                    <>
                        <label 
                            htmlFor="grade"
                        >
                            {grade}
                        </label>

                        <input 
                            type="radio"
                            name="grade"
                            value={grade.toString()}
                            onChange={(e) => onChange(Number(e.currentTarget.value) as any)}
                            className={styles.radio}
                        />
                    </>
                ))}
            </div>
        </div>
    )
}

export default SelectGrade;
