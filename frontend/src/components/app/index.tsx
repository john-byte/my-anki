import React from "react";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";

import { applyTheme, DefaultTheme } from "../common/themes";
import ChooseDeck from "../choose-deck";
import RandomCard from "../random-card";
import FinishScreen from "../finish-screen";
import RegisterScreen from "../register-screen";
import LoginScreen from "../login-screen";
import Header from "../header";
import PassRecoveryScreen from "../pass-recovery-screen";
import NewPasswordScreen from "../new-password-screen";
import DashboardScreen from "../dashboard-screen";

const GlobalStyle = createGlobalStyle`
    body {
        background-color: ${
            props => applyTheme({
                // @ts-ignore 
                theme: props.theme 
            }).colors.background
        };
    }
`;

const withHeader = (Component: React.FC) => () => {
    return (
        <>
            <Header />
            <Component />
        </>
    );
};

// TODO: add authorization wrapper for some components
const App: React.FC = () => {
    return (
        <ThemeProvider theme={DefaultTheme}>
            <Router>
                <Switch>
                    <Route exact path="/deck/:name" component={withHeader(RandomCard)}/>
                    <Route exact path="/finish" component={withHeader(FinishScreen)} />
                    <Route exact path="/register" component={RegisterScreen} />
                    <Route exact path="/login" component={LoginScreen} />
                    <Route exact path="/pass-recovery" component={PassRecoveryScreen} />
                    <Route exact path="/new-password" component={NewPasswordScreen} />
                    <Route exact path="/dashboard" component={withHeader(DashboardScreen)} />
                    <Route path="/" component={withHeader(ChooseDeck)} /> 
                </Switch>

                <GlobalStyle />
            </Router>
        </ThemeProvider>
    )
};

export default App;
