import React from "react";
import { useHistory } from "react-router";
import styled from "styled-components";
import { H1, P } from "../common/Text";
import { Row, Col } from "../common/Grid";
import { BACKEND_URL } from "../../constants";
import DeckButton from "./deck-button";
import { Deck } from "../../types";
import { WidthContainer } from "../common/WidthContainer";
import { VertCenterer } from "../common/VertCenterer";

const fetchDecks = async (): Promise<Deck[]> => {
    const sessCode = localStorage.getItem("sesscode");
    if (typeof sessCode !== "string" || !sessCode) {
        throw Error("not-auth");
    }
    
    const res = await fetch(
        `${BACKEND_URL}/anki/decks`,
        {
            method: "GET",
            headers: {
                "content-type": "application/json",
                "x-sesscode": sessCode,
            }
        }
    );

    if (res.status === 403) {
        throw Error("not-auth");
    }

    const data = await res.json();
    if (!Array.isArray(data)) {
        throw Error("Invalid response");
    }

    return data;
};

const RowWithGaps = styled(Row)`
    .child {
        margin-top: 32px;

        &:not(:nth-child(3n)) {
            margin-right: 32px;
        }
    }
`;

const ChooseDeck: React.FC = () => {
    const history = useHistory();

    const [pending, setPending] = React.useState(true);
    const [decks, setDecks] = React.useState<Deck[]>();
    const [error, setError] = React.useState<Error>();

    React.useEffect(
        () => {
            setPending(true);

            fetchDecks().then(
                data => {
                    setDecks(data);    
                }
            ).catch(
                err => {
                    if (err?.message === "not-auth") {
                        history.push("/login");
                    }

                    setError(err);
                })
            .finally(
                () => {
                    setPending(false);
                }
            )
        },
        []
    );

    return (
        <WidthContainer style={{ maxWidth: '768px' }}>
            <VertCenterer style={{ alignItems: 'center' }}>
                {
                    pending ? (
                        <H1>
                            Loading decks...
                        </H1>
                    ) : decks ? (
                        <>
                            <H1>
                                Choose deck
                            </H1>

                            <RowWithGaps justify='center'>
                                {decks.map(
                                    deck => (
                                        <DeckButton 
                                            deckName={deck.uid}
                                            title={deck.title}
                                            className='child'
                                        />
                                    )
                                )}
                            </RowWithGaps>
                        </>
                    ) : error ? (
                        <>
                            <H1>
                                Error during loading decks:
                            </H1>

                            <P>
                                {error.message}
                            </P>
                        </>
                    ) : null
                }
            </VertCenterer>
        </WidthContainer>
    ) 
}

export default ChooseDeck;
