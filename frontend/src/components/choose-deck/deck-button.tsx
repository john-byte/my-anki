import React from "react";
import styled from "styled-components";
import { useHistory } from "react-router";
import { Button } from "../common/Button";
import { shortenString } from "../../utils";
import { Tooltip } from "../common/Tooltip";

interface Props {
    deckName: string;
    title: string;
    className?: string;
}

const Wrapper = styled(Button.Primary)`
    width: 200px;
    height: 50px;
`;

const DeckButton: React.FC<Props> = ({ deckName, title, className }) => {
    const history = useHistory();
    
    const onClick = React.useCallback(
        () => {
            history.push(
                `/deck/${deckName}`
            );
        },
        [deckName]
    );
    
    return (
        <Tooltip
            className={`${className}`}
            content={title || deckName}
        >
            <Wrapper onClick={onClick}>
                {shortenString(title || deckName, 20, "..")}
            </Wrapper>
        </Tooltip>
    )
}

export default DeckButton;
