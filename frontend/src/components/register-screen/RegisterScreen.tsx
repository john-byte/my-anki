import React, { useState, useMemo } from "react";
import { useHistory } from "react-router";
import styled from "styled-components";
import { WidthContainer } from "../common/WidthContainer";
import { VertCenterer } from "../common/VertCenterer";
import { Card } from "../common/Card";
import { Label } from "../common/Label";
import { Input } from "../common/Input";
import { Button } from "../common/Button";
import { Row } from "../common/Grid";
import { PError } from "../common/Text";
import { Dict } from '../../types';
import { useForm } from "../../hooks";
import { BACKEND_URL } from "../../constants";
import { makeFormSubmit } from "../../utils/makeFormSubmit";

import Success from "./Success";

const Stack = styled(Card)`
    & > .child + .child {
        margin-top: 8px;
    }
`;

const emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
const formFields = {
    "nickname": {
        validate: (v: string) => {
            return v.length >= 3 && v.length <= 16;
        },
        errorMessage: "Nickname is too short",
    },
    "email": {
        validate: (v: string) => {
            return emailRegex.test(v);
        },
        errorMessage: "Invalid email",
    },
    "password": {
        validate: (v: string) => {
            return v.length >= 8;
        },
        errorMessage: "Password is too short",
    },
    "repPassword": {
        validate: (v: string, fields?: Dict<string>) => {
            return fields?.["password"] === v;
        },
        errorMessage: "Passwords must match",
    },
};

const RegisterScreen = () => {
    const history = useHistory();
    const [onSuccessScreen, setOnSuccessScreen] = useState(false);
    const handleSuccess = () => {
        setOnSuccessScreen(true);
    };

    const [pending, setPending] = useState(false);
    const [nicknameAlreadyTakenError, setNicknameAlreadyTakenError] = useState<string>();
    const [emailAlreadyTakenError, setEmailAlreadyTakenError] = useState<string>();
    const handleError = (e: any) => {
        if (e.includes("already registered")) {
            if (e.includes("nickname")) {
                setNicknameAlreadyTakenError(e);
            } else if (e.includes("email")) {
                setEmailAlreadyTakenError(e);
            }
        }
    };
    const clearServiceErrors = () => {
        setNicknameAlreadyTakenError(undefined);
        setEmailAlreadyTakenError(undefined);
    };

    const formSubmit = useMemo(() => {
        return makeFormSubmit(
            `${BACKEND_URL}/auth/register`,
            (fields: Dict<string>) => {
                const req: any = Object.assign({}, {
                    ...fields,
                    username: fields["nickname"],
                });
                delete req["repPassword"];
                return req;
            },
            handleSuccess,
            handleError,
            (resData: any) => {
                return !!resData["success"];
            },
            "Can't register. Try again later",
            () => setPending(true),
            () => setPending(false),
        );
    }, []);

    const { getValue, setValue, submit, getError } = useForm({
        fields: formFields,
        onSubmit: formSubmit,
        onFieldChange: clearServiceErrors,
    });

    return (
        <WidthContainer style={{ maxWidth: '640px' }}>
            <VertCenterer>
                {!onSuccessScreen ? (
                    <Stack>
                        <div className="child">
                            <Label>{'Nickname (>= 3 and <= 16 characters)'}</Label>
                            <Input 
                                value={getValue("nickname")}
                                onChange={(e) => setValue("nickname", e.currentTarget.value)}
                            />
                            {(() => {
                                const err = getError("nickname");

                                return <PError>{err || nicknameAlreadyTakenError}</PError>;
                            })()}
                        </div>

                        <div className="child">
                            <Label>{'Email'}</Label>
                            <Input 
                                value={getValue("email")}
                                onChange={(e) => setValue("email", e.currentTarget.value)}
                            />
                            {(() => {
                                const err = getError("email");

                                return <PError>{err || emailAlreadyTakenError}</PError>;
                            })()}
                        </div>

                        <div className="child">
                            <Label>{'Password'}</Label>
                            <Input 
                                value={getValue("password")}
                                onChange={(e) => setValue("password", e.currentTarget.value)}
                                type="password"
                            />
                            {(() => {
                                const err = getError("password");

                                return <PError>{err}</PError>;
                            })()}
                        </div>

                        <div className="child">
                            <Label>{'Repeat password'}</Label>
                            <Input 
                                value={getValue("repPassword")}
                                onChange={(e) => setValue("repPassword", e.currentTarget.value)}
                                type="password"
                            />
                            {(() => {
                                const err = getError("repPassword");

                                return <PError>{err}</PError>;
                            })()}
                        </div>

                        <div style={{ marginTop: '20px' }}>
                            <Row style={{ justifyContent: 'space-between' }}>
                                <Button.Primary onClick={submit} disabled={pending}>
                                    {pending ? '.....' : 'Submit'}
                                </Button.Primary>

                                <Button.Secondary onClick={() => history.push("/login")}>
                                    Login
                                </Button.Secondary>
                            </Row>
                        </div>
                    </Stack>
                ) : (
                    <Success />
                )}
            </VertCenterer>
        </WidthContainer>
    )
}

export default RegisterScreen;
