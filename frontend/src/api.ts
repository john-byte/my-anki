import { BACKEND_URL } from "./constants";

export const fetchCardSides = async (deckName: string, cardName: string) => {
    const sessCode = localStorage.getItem("sesscode");
    if (typeof sessCode !== "string" || !sessCode) {
        throw Error("not-auth");
    }

    const res = await fetch(
        `${BACKEND_URL}/anki/card`,
        {
            method: "POST",
            headers: {
                "content-type": "application/json",
                "x-sesscode": sessCode,
            },
            body: JSON.stringify({
                deck_name: deckName,
                card_name: cardName
            })
        }
    );

    if (res.status === 403) {
        throw Error("not-auth");
    }

    return res.json();
}