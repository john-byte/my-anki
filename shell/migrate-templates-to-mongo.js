const fs = require('fs').promises;
const path = require('path');
const MongoClient = require("mongodb").MongoClient;

const readCards = async () => {
    const decksToCards = {};

    const decksPath = path.resolve(
        __dirname,
        "..",
        process.env.PATH_RESOURCES,
        process.env.PATH_DECKS,
    );
    const decks = await fs.readdir(decksPath);

    for (const deck of decks) {
        const cardsPath = path.resolve(decksPath, deck);
        const cards = await fs.readdir(cardsPath);

        for (const card of cards) {
            const frontPath = path.resolve(cardsPath, card, "front.html");
            const backPath = path.resolve(cardsPath, card, "back.html");
            const frontSide = await fs.readFile(frontPath, { encoding: 'utf-8' });
            const backSide = await fs.readFile(backPath, { encoding: 'utf-8' });

            if (!decksToCards[deck]) {
                decksToCards[deck] = [];
            }
            decksToCards[deck].push({
                uid: card,
                deckUid: deck,
                author: 'admin',
                frontSide,
                backSide,
            });
        }
    }

    return decksToCards;
};

const connectToMongo = async () => {
    return await MongoClient.connect(process.env.MONGODB_URL);
};

const insertTemplates = async (decksToCards, collection) => {
    const transactions = [];
    
    for (const deck in decksToCards) {
        for (const templatePair of decksToCards[deck]) {
            transactions.push({
                insertOne: templatePair
            });
        }
    }

    await collection.bulkWrite(transactions);
};

const main = async () => {
    const decksToCards = await readCards();
    const dbClient = await connectToMongo();
    const db = dbClient.db('anki-templates');
    await insertTemplates(decksToCards, db.collection("templates"));
    await dbClient.close();
}

main().catch(err => console.error(err));
